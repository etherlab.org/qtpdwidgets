/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_GRAPH_H
#define PD_GRAPH_H

#include "Export.h"
#include <QtPdCom1/Transmission.h>

#include <pdcom5/Variable.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscription.h>

#include <QFrame>
#include <QIcon>
#include <QAction>

namespace Pd {

/****************************************************************************/

/** Graph widget.
 */
class PD_PUBLIC Graph: public QFrame
{
    Q_OBJECT
    Q_ENUMS(Mode TriggerLevelMode)
    Q_PROPERTY(Mode mode
            READ getMode WRITE setMode RESET resetMode)
    /** The time range to display. */
    Q_PROPERTY(double timeRange
            READ getTimeRange WRITE setTimeRange RESET resetTimeRange)
    /** The minimum value on the value scale. */
    Q_PROPERTY(double scaleMin
            READ getScaleMin WRITE setScaleMin RESET resetScaleMin)
    /** The maximum value on the value scale. */
    Q_PROPERTY(double scaleMax
            READ getScaleMax WRITE setScaleMax RESET resetScaleMax)
    Q_PROPERTY(TriggerLevelMode triggerLevelMode
            READ getTriggerLevelMode WRITE setTriggerLevelMode
            RESET resetTriggerLevelMode)
    Q_PROPERTY(double manualTriggerLevel
            READ getManualTriggerLevel WRITE setManualTriggerLevel
            RESET resetManualTriggerLevel)
    Q_PROPERTY(double triggerPosition
            READ getTriggerPosition WRITE setTriggerPosition
            RESET resetTriggerPosition)
    Q_PROPERTY(double triggerTimeout
            READ getTriggerTimeout WRITE setTriggerTimeout
            RESET resetTriggerTimeout)
    Q_PROPERTY(QString suffix
            READ getSuffix WRITE setSuffix RESET resetSuffix)
    Q_PROPERTY(QColor gridColor
            READ getGridColor WRITE setGridColor RESET resetGridColor)
    Q_PROPERTY(bool autoScaleWidth
            READ getAutoScaleWidth WRITE setAutoScaleWidth
            RESET resetAutoScaleWidth)

    public:
        Graph(QWidget *parent = 0);
        virtual ~Graph();

        /** Subscribe to a process variable.
         */
        void setVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Graph color. */
                );
        void setVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Graph color. */
                );
        void addVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Graph color. */
                );
        void addVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Graph color. */
                );
        void clearVariables();
        void clearData();

        /** Subscribe to a process variable as trigger variable.
         */
        void setTriggerVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                   or equal to 0.0 means, that no filter is
                                   applied. */
                );
        /** Subscribe to a process variable as trigger variable.
         */
        void setTriggerVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                   or equal to 0.0 means, that no filter is
                                   applied. */
                );
        void clearTriggerVariable();

        /** Graph mode.
         *
         * The general behaviour of the graph.
         */
        enum Mode {
            Roll, /**< Roll mode. The right border of the widget always
                    represents the current time, so the displayed data values
                    'roll' over the widget from right to left. */
            Trigger /**< Trigger mode. The data are recorded in a circular
                      buffer and analyzed continuously. If the trigger
                      condition is fulfilled, the recording is continued,
                      until there are enough data in the buffer to display a
                      complete snapshot. */
        };
        Mode getMode() const;
        void setMode(Mode);
        void resetMode();
        double getTimeRange() const;
        void setTimeRange(double);
        void resetTimeRange();
        double getScaleMin() const;
        void setScaleMin(double);
        void resetScaleMin();
        double getScaleMax() const;
        void setScaleMax(double);
        void resetScaleMax();
        /** Trigger level mode.
         *
         * Determines, how the trigger level is generated.
         */
        enum TriggerLevelMode {
            AutoLevel, /**< Automatic trigger level calculation. */
            ManualLevel /**< Use the #manualTriggerLevel property. */
        };
        TriggerLevelMode getTriggerLevelMode() const;
        void setTriggerLevelMode(TriggerLevelMode);
        void resetTriggerLevelMode();
        double getManualTriggerLevel() const;
        void setManualTriggerLevel(double);
        void resetManualTriggerLevel();
        double getTriggerPosition() const;
        void setTriggerPosition(double);
        void resetTriggerPosition();
        double getTriggerTimeout() const;
        void setTriggerTimeout(double);
        void resetTriggerTimeout();
        QString getSuffix() const;
        void setSuffix(const QString &);
        void resetSuffix();
        QColor getGridColor() const;
        void setGridColor(const QColor &);
        void resetGridColor();
        bool getAutoScaleWidth() const;
        void setAutoScaleWidth(bool);
        void resetAutoScaleWidth();

        Mode getEffectiveMode() const;

        virtual QSize sizeHint() const;

        /** Graph state.
         */
        enum State {
            Run, /**< Incoming data shall be displayed immediately. */
            Stop, /**< Incoming data shall be processed in background, but the
                    display shall not be changed. */
        };
        bool getState() const;
        void setState(State);
        void toggleState();

    protected:
        bool event(QEvent *) override;
        void resizeEvent(QResizeEvent *) override;
        void paintEvent(QPaintEvent *) override;
        void contextMenuEvent(QContextMenuEvent *) override;

    private slots:
        PD_PRIVATE void redrawEvent();
        PD_PRIVATE void run();
        PD_PRIVATE void stop();

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

        class PD_PRIVATE Layer;
        friend class Layer;

        PD_PRIVATE void setRedraw();
        PD_PRIVATE void notifySampled();

        class PD_PRIVATE TriggerDetector;
        friend class TriggerDetector;

        PD_PRIVATE void triggerConditionDetected(std::chrono::nanoseconds);
        PD_PRIVATE void triggerIdle();
};

/****************************************************************************/

} // namespace

#endif
