/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_XYGRAPH_H
#define PD_XYGRAPH_H

#include "Export.h"
#include <QtPdCom1/Transmission.h>

#include <pdcom5/Variable.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscription.h>

#include <QFrame>

#include <memory>

namespace Pd {

/****************************************************************************/

/** XY Graph.
 */
class PD_PUBLIC XYGraph:
    public QFrame
{
    Q_OBJECT
    /** The time range to display. Older points will be discarded. */
    Q_PROPERTY(double timeRange
            READ getTimeRange WRITE setTimeRange RESET resetTimeRange)
    /** The minimum value of the horizontal scale. */
    Q_PROPERTY(double scaleXMin
            READ getScaleXMin WRITE setScaleXMin RESET resetScaleXMin)
    /** The maximum value of the horizontal scale. */
    Q_PROPERTY(double scaleXMax
            READ getScaleXMax WRITE setScaleXMax RESET resetScaleXMax)
    /** The minimum value of the vertical scale. */
    Q_PROPERTY(double scaleYMin
            READ getScaleYMin WRITE setScaleYMin RESET resetScaleYMin)
    /** The maximum value of the vertical scale. */
    Q_PROPERTY(double scaleYMax
            READ getScaleYMax WRITE setScaleYMax RESET resetScaleYMax)
    /** The line width. */
    Q_PROPERTY(int lineWidth
            READ getLineWidth WRITE setLineWidth RESET resetLineWidth)
    /** The line color. */
    Q_PROPERTY(QColor lineColor
            READ getLineColor WRITE setLineColor
            RESET resetLineColor)

    public:
        XYGraph(QWidget *parent = 0);
        virtual ~XYGraph();

        void addVariable(PdCom::Variable, const PdCom::Selector & = {},
                const QtPdCom::Transmission & = QtPdCom::event_mode, double = 1.0,
                double = 0.0);
        void addVariable(PdCom::Process *, const QString &,
                const PdCom::Selector & = {},
                const QtPdCom::Transmission & = QtPdCom::event_mode, double = 1.0,
                double = 0.0);
        void clearVariables();
        void clearData();

        double getTimeRange() const;
        void setTimeRange(double);
        void resetTimeRange();
        double getScaleXMin() const;
        void setScaleXMin(double);
        void resetScaleXMin();
        double getScaleXMax() const;
        void setScaleXMax(double);
        void resetScaleXMax();
        double getScaleYMin() const;
        void setScaleYMin(double);
        void resetScaleYMin();
        double getScaleYMax() const;
        void setScaleYMax(double);
        void resetScaleYMax();
        int getLineWidth() const;
        void setLineWidth(int);
        void resetLineWidth();
        QColor getLineColor() const;
        void setLineColor(const QColor &);
        void resetLineColor();

        virtual QSize sizeHint() const;

    protected:
        bool event(QEvent *);
        void resizeEvent(QResizeEvent *);
        void paintEvent(QPaintEvent *);

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

} // namespace

#endif
