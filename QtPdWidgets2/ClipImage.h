/*****************************************************************************
 *
 * Copyright (C) 2019  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_CLIPIMAGE_H
#define PD_CLIPIMAGE_H

#include <QFrame>

#include "Export.h"
#include <QtPdCom1/ScalarSubscriber.h>

namespace Pd {

/****************************************************************************/

class PD_PUBLIC ClipImage:
    public QFrame, QtPdCom::ScalarSubscriber
{
    Q_OBJECT
    Q_ENUMS(ClipMode)
    Q_PROPERTY(QString background
            READ getBackground WRITE setBackground RESET resetBackground)
    Q_PROPERTY(QString foreground
            READ getForeground WRITE setForeground RESET resetForeground)
    Q_PROPERTY(ClipMode clipMode
            READ getClipMode WRITE setClipMode RESET resetClipMode)

    public:
        ClipImage(QWidget * = 0);
        ~ClipImage();

        QSize sizeHint() const;

        QString getBackground() const;
        void setBackground(const QString &);
        void resetBackground();

        QString getForeground() const;
        void setForeground(const QString &);
        void resetForeground();

        enum ClipMode {
            Bar,
            Clock
        };
        ClipMode getClipMode() const;
        void setClipMode(ClipMode);
        void resetClipMode();

    private:
        class PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

        PD_PRIVATE void resizeEvent(QResizeEvent *) override;
        PD_PRIVATE void paintEvent(QPaintEvent *) override;

        PD_PRIVATE void newValues(std::chrono::nanoseconds) override;
        PD_PRIVATE void stateChange(PdCom::Subscription::State) override;
};

} // namespace

/****************************************************************************/

#endif
