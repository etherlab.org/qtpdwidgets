/*****************************************************************************
 *
 * Dial / analog gauge.
 *
 * Copyright (C) 2012 - 2017  Richard Hacker <ha@igh.de>
 *                            Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_DIAL_H
#define PD_DIAL_H

#include <QtPdCom1/ScalarSubscriber.h>
#include "Export.h"

#include <QFrame>
#include <QPixmap>
#include <QTimer>
#include <QElapsedTimer>

class QEvent;
class QWidget;
class QMouseEvent;

namespace Pd {

/****************************************************************************/

class PD_PUBLIC Dial: public QFrame
{
    Q_OBJECT

    Q_PROPERTY(QString title READ getTitle WRITE setTitle RESET resetTitle)
    Q_PROPERTY(QString unit READ getUnit WRITE setUnit RESET resetUnit)
    Q_PROPERTY(double span READ getSpan WRITE setSpan RESET resetSpan)
    Q_PROPERTY(double scaleMin READ getScaleMin WRITE setScaleMin
            RESET resetScaleMin)
    Q_PROPERTY(double majorStep READ getMajorStep WRITE setMajorStep
            RESET resetMajorStep)
    Q_PROPERTY(unsigned int majorStops READ getMajorStops WRITE setMajorStops
            RESET resetMajorStops)
    Q_PROPERTY(unsigned int minorStops READ getMinorStops WRITE setMinorStops
            RESET resetMinorStops)
    Q_PROPERTY(QColor pieColor READ getPieColor WRITE setPieColor
            RESET resetPieColor)

    public:
        Dial(QWidget * = 0);
        ~Dial();

        QString getTitle() const;
        void setTitle(const QString &);
        void resetTitle();

        QString getUnit() const;
        void setUnit(const QString &);
        void resetUnit();

        double getSpan() const;
        void setSpan(double);
        void resetSpan();

        double getScaleMin() const;
        void setScaleMin(double);
        void resetScaleMin();

        double getMajorStep() const;
        void setMajorStep(double);
        void resetMajorStep();

        unsigned int getMajorStops() const;
        void setMajorStops(unsigned int);
        void resetMajorStops();

        unsigned int getMinorStops() const;
        void setMinorStops(unsigned int);
        void resetMinorStops();

        const QColor &getPieColor() const;
        void setPieColor(const QColor &);
        void resetPieColor();

        class Value:
            public QtPdCom::ScalarSubscriber
        {
            public:
                Value(Dial *dial);

                bool hasData() const;
                double getValue() const;

            private:
                struct PD_PRIVATE Impl;
                std::unique_ptr<Impl> impl;

                PD_PRIVATE void stateChange(PdCom::Subscription::State) override;
                PD_PRIVATE void newValues(std::chrono::nanoseconds) override;
        }
        currentValue, setpointValue;

        void setNeedle(const QString &);
        void setNeedleCenterX(int);

        void setSetpoint(const QString &);

        void setGradientStops(const QGradientStops &);

    signals:
        void setpointChanged(double);

    protected:
        QSize sizeHint() const override;
        bool event(QEvent *) override;
        void paintEvent(QPaintEvent *) override;

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

    private slots:
        PD_PRIVATE void redrawEvent();
};

/****************************************************************************/

} // namespace

#endif
