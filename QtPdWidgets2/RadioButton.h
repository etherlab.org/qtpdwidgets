/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_RADIOBUTTON_H
#define PD_RADIOBUTTON_H

#include <QRadioButton>

#include "Export.h"
#include <QtPdCom1/ScalarSubscriber.h>

namespace Pd {

/****************************************************************************/

/** RadioButton.
 */
class PD_PUBLIC RadioButton:
    public QRadioButton, public QtPdCom::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(int checkValue
            READ getCheckValue WRITE setCheckValue RESET resetCheckValue)

    public:
        RadioButton(QWidget *parent = 0);
        virtual ~RadioButton();

        void clearData(); // pure-virtual from ScalarSubscriber

        int getCheckValue() const;
        void setCheckValue(int);
        void resetCheckValue();

    protected:
        void nextCheckState();

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

        PD_PRIVATE void newValues(std::chrono::nanoseconds) override;
};

/****************************************************************************/

} // namespace

#endif
