/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SVG_H
#define PD_SVG_H

#include <QFrame>
#include <QString>
#include <QScopedPointer>

#include "Export.h"

namespace Pd {

class SvgPrivate;

/****************************************************************************/

/** Svg widget.
 */

class PD_PUBLIC Svg:
    public QFrame
{
    Q_OBJECT

    Q_PROPERTY(QString svgPath READ getSvgPath WRITE setSvgPath RESET resetSvgPath)
    Q_PROPERTY(qreal angle READ getAngle WRITE setAngle RESET resetAngle)

    public:
        Svg(QWidget * = 0);
        ~Svg();

        QString getSvgPath() const;
        void setSvgPath(const QString &);
        void resetSvgPath();

        qreal getAngle() const;
        void setAngle(qreal);
        void resetAngle();

        void setIdList(const QStringList &);
        QStringList getIdList() const;
        bool existId(QString) const;

    private:
        Q_DECLARE_PRIVATE(Svg)
        Q_DISABLE_COPY(Svg)

        QScopedPointer<SvgPrivate> const d_ptr;

        void paintEvent(QPaintEvent *);
};

} // namespace Pd

#endif // PD_SVG_H
