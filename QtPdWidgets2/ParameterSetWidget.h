/*****************************************************************************
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *               2023 Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_PARAMETERSETWIDGET_H
#define PD_PARAMETERSETWIDGET_H

#include <QtPdCom1/Process.h>

#include "Export.h"

#include <QFrame>

#include <memory>

namespace Pd {

/****************************************************************************/

/** Quick-selector widget for parameter sets.
 *
 * The ParameterSetWidget class provides functionality for loading and
 * applying parameter files. It displays a list of parameter files in a combo
 * box, and allows the user to select a file to load or apply the parameters
 * to the real-time process. To use it, a set of process pointers
 * (Pd::Process *) has to be passed to the widget.
 *
 * The parameter file format is JSON-based and is also understood by the
 * Testmanager directory:
 *
 *     {
 *        "parameters":[
 *           {
 *              "url":"msr://host:2345/control/timeout",
 *              "value":13,
 *              "comment":"Scalar Parameter, Timeout in [s]"
 *           },
 *           {
 *              "url":"msr://host:2345/pump/speed",
 *              "value":1500,
 *              "scale":9.549296585513721,
 *              "comment":"Scaled Parameter in [rpm], for easier user editing. Process value * scale = file value."
 *           },
 *           {
 *              "url":"msr://anotherhost:2345/ForceController/TargetForce",
 *              "value":50e3,
 *              "selector":1,
 *              "comment":"Vector parameter in [N], selecting 2nd component"
 *           },
 *           {
 *              "url":"msr://anotherhost:2345/Matrix2x2",
 *              "value":[
 *                 1.0,
 *                 1.0
 *              ],
 *              "selector":[
 *                 0,
 *                 3
 *              ],
 *              "comment":"Matrix parameter, selecting diagonal"
 *           }
 *        ],
 *        "creator":"Testmanager 0.1.34-g37baaf4d",
 *        "creation date":"2023-03-13 14:37:58+0100",
 *        "created by":"fp",
 *        "description":"My settings"
 *     }
 */
class PD_PUBLIC ParameterSetWidget:
    public QFrame
{
    Q_OBJECT

    Q_PROPERTY(QString path READ getPath WRITE setPath)

    public:
        ParameterSetWidget(QWidget *parent = nullptr);
        virtual ~ParameterSetWidget();

        void setProcesses(QSet<QtPdCom::Process *> processes);

        void setPath(const QString &);
        QString getPath() const;

    protected:
        bool event(QEvent *) override;
        QSize sizeHint() const override;

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

} // namespace

#endif
