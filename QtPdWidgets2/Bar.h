/*****************************************************************************
 *
 * Copyright (C) 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_BAR_H
#define PD_BAR_H

#include <QWidget>
#include <QPixmap>

#include "Export.h"

#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/Transmission.h>

#include <QScopedPointer>

namespace Pd {

class BarPrivate;

/****************************************************************************/

/** Bar graph widget.
 */
class PD_PUBLIC Bar: public QWidget
{
    Q_OBJECT
    Q_ENUMS(Orientation Style Origin)
    Q_PROPERTY(Orientation orientation
            READ getOrientation WRITE setOrientation RESET resetOrientation)
    Q_PROPERTY(Style style
            READ getStyle WRITE setStyle RESET resetStyle)
    /** \deprecated */
    Q_PROPERTY(bool showScale
            READ getShowScale WRITE setShowScale RESET resetShowScale)
    /** \see setScaleMin() */
    Q_PROPERTY(double scaleMin
            READ getScaleMin WRITE setScaleMin RESET resetScaleMin)
    /** \see setScaleMax() */
    Q_PROPERTY(double scaleMax
            READ getScaleMax WRITE setScaleMax RESET resetScaleMax)
    Q_PROPERTY(Origin origin
            READ getOrigin WRITE setOrigin RESET resetOrigin)
    Q_PROPERTY(int borderWidth
            READ getBorderWidth WRITE setBorderWidth RESET resetBorderWidth)
    Q_PROPERTY(QColor backgroundColor
            READ getBackgroundColor WRITE setBackgroundColor
            RESET resetBackgroundColor)
    Q_PROPERTY(bool autoBarWidth
            READ getAutoBarWidth WRITE setAutoBarWidth
            RESET resetAutoBarWidth)

    public:
        Bar(QWidget *parent = 0);
        virtual ~Bar();

        /** Orientation of the bar widget. */
        enum Orientation  {
            Vertical, /**< Vertical bar with scale on the left. */
            Horizontal /**< Horizontal bar with scale on top. */
        };
        Orientation getOrientation() const;
        void setOrientation(Orientation);
        void resetOrientation();

        /** Style of the bar widget. */
        enum Style  {
            ColorBar, /**< Display colored bars over a plain background. */
            Arrow, /**< Display arrows over a colored background. */
            MultiColorBar /**< Display solid colored bar acc. to value. */
        };
        Style getStyle() const;
        void setStyle(Style);
        void resetStyle();

        bool getShowScale() const;
        void setShowScale(bool);
        void resetShowScale();

        double getScaleMin() const;
        void setScaleMin(double);
        void resetScaleMin();

        double getScaleMax() const;
        void setScaleMax(double);
        void resetScaleMax();

        /** Bar origin mode.
         *
         * This determines, from where bars are drawn.
         */
        enum Origin {
            OriginZero, /**< Draw bars originating from zero. */
            OriginMinimum, /**< Draw bars originating from the scale minimum.
                             If #orientation is set to #Vertical, this is the
                             bottom of the bar area, while with #orientation
                             set to #Horizontal, this is the left side of the
                             bar area. */
            OriginMaximum, /**< Draw bars originating from the scale maximum.
                             If #orientation is set to #Vertical, this is the
                             top of the bar area, while with #orientation set
                             to #Horizontal, this is the right side of the bar
                             area. */
        };
        Origin getOrigin() const;
        void setOrigin(Origin);
        void resetOrigin();

        int getBorderWidth() const;
        void setBorderWidth(int);
        void resetBorderWidth();

        QColor getBackgroundColor() const;
        void setBackgroundColor(QColor);
        void resetBackgroundColor();

        bool getAutoBarWidth() const;
        void setAutoBarWidth(bool);
        void resetAutoBarWidth();

        QSize sizeHint() const override;

        /** Connect to a process variable using a new bar stack.
         *
         * A new stack (horizontal division for veritcal bar orientation) is
         * created. A stack can display multiple sections on top of each other
         * (speaking for vertical bar orientation). To add a new section to an
         * existing stack, use addStackedVariable().
         *
         * \see ScalarSubscriber::setVariable().
         */
        void addVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Bar color. */
                );
        void addVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Bar color. */
                );

        /** Connect to a process variable using the last stack.
         *
         * The variable is placed as upper element of the last stack. If
         * no stacks exist, the forst one is created.
         *
         * \see addVariable().
         * \see ScalarSubscriber::setVariable().
         */
        void addStackedVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling. */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Bar color. */
                );
        void addStackedVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling. */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Bar color. */
                );

        /** Clear all stacks and sections.
         */
        void clearVariables();

        /** Clear all stacks and sections and add a single variable.
         *
         * \deprecated
         * \see addVariable()
         */
        void setVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Bar color. */
                );
        void setVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0, /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                QColor color = Qt::blue /**< Bar color. */
                );
        void clearData();

        void setGradientStops(const QGradientStops &);
        const QPair<double, double> getGradientLimits() const;

    protected:
        bool event(QEvent *) override;
        void resizeEvent(QResizeEvent *) override;
        void paintEvent(QPaintEvent *event) override;

    private:
        Q_DECLARE_PRIVATE(Bar)

        QScopedPointer<BarPrivate> const d_ptr;
};

/****************************************************************************/


} // namespace Pd

#endif
