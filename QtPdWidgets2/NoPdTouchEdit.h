/*****************************************************************************
 *
 * Copyright (C) 2011 - 2012  Andreas Stewering-Bone <ab@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef NOPD_TOUCHEDIT_H
#define NOPD_TOUCHEDIT_H

#include <QFrame>
#include <QScopedPointer>

#include "Export.h"

namespace Pd {

class TouchEditDialog;

class NoPdTouchEditPrivate;

/****************************************************************************/

/** Touch edit for non-process data.
 */
class PD_PUBLIC NoPdTouchEdit:
    public QFrame
{
    Q_OBJECT

    Q_PROPERTY(double value
            READ getValue WRITE setValue RESET resetValue)
    Q_PROPERTY(Qt::Alignment alignment
            READ getAlignment WRITE setAlignment RESET resetAlignment)
    Q_PROPERTY(quint32 decimals
            READ getDecimals WRITE setDecimals RESET resetDecimals)
    Q_PROPERTY(QString suffix
            READ getSuffix WRITE setSuffix RESET resetSuffix)
    Q_PROPERTY(double lowerLimit
            READ getLowerLimit WRITE setLowerLimit
            RESET resetLowerLimit)
    Q_PROPERTY(double upperLimit
            READ getUpperLimit WRITE setUpperLimit
            RESET resetUpperLimit)

    public:
        NoPdTouchEdit(QWidget *parent = 0);
        virtual ~NoPdTouchEdit();

        double getValue() const;
        void setValue(double);
        void resetValue();

        Qt::Alignment getAlignment() const;
        void setAlignment(Qt::Alignment);
        void resetAlignment();

        quint32 getDecimals() const;
        void setDecimals(quint32);
        void resetDecimals();

        QString getSuffix() const;
        void setSuffix(const QString &);
        void resetSuffix();

        double getLowerLimit() const;
        void setLowerLimit(double);
        void resetLowerLimit();

        double getUpperLimit() const;
        void setUpperLimit(double);
        void resetUpperLimit();

        QSize sizeHint() const;

    signals:
        void valueChanged();

    protected:
        bool event(QEvent *) override;
        void paintEvent(QPaintEvent *) override;
        void drawText(QPaintEvent *, QPainter &);
        void changeEvent(QEvent *) override;

    private:
        Q_DECLARE_PRIVATE(NoPdTouchEdit)
        QScopedPointer<NoPdTouchEditPrivate> const d_ptr;
};

/****************************************************************************/

} // namespace

#endif
