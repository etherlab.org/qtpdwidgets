/*****************************************************************************
 *
 * Copyright (C) 2009 - 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_SENDBROADCASTWIDGET_H
#define PD_SENDBROADCASTWIDGET_H

#include "Export.h"

#include <QScopedPointer>
#include <QString>
#include <QWidget>

class QLineEdit;
class QPushButton;

namespace QtPdCom {
class Process;
}

namespace Pd {

class SendBroadcastWidgetPrivate;

/** \brief Widget to send broadcast messages.
 *
 * This widget contains a line edit and a send button to send broadcasts to all
 * msr clients.
 * Note that broadcasts have to be enabled in pdserv.
 *
 */
class PD_PUBLIC SendBroadcastWidget : public QWidget
{
    Q_OBJECT
  public:
    explicit SendBroadcastWidget(QWidget *parent = nullptr);
    ~SendBroadcastWidget();

    /** \brief Name of the xml attribute.
     *
     * Default \c text , there is no need to change that.
     */
    Q_PROPERTY(
            QString attributeName READ getAttributeName WRITE setAttributeName)


    /** \brief Set the Process.
     *
     * \param p Process, can be NULL.
     */
    void setProcess(QtPdCom::Process *p);

    QtPdCom::Process *getProcess() const;

    QString getAttributeName() const;
    void setAttributeName(QString name);

  private:
    Q_DECLARE_PRIVATE(SendBroadcastWidget)

    QScopedPointer<SendBroadcastWidgetPrivate> const d_ptr;

    PD_PRIVATE void changeEvent(QEvent *event) override;
};

}  // namespace Pd

#endif  // PD_SENDBROADCASTWIDGET_H
