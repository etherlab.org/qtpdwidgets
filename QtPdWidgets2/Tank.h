/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_TANK_H
#define PD_TANK_H

#include <QtPdCom1/Transmission.h>

#include <pdcom5/Variable.h>
#include <pdcom5/Subscription.h>

#include <QFrame>

#include "Export.h"
#include <memory>

namespace Pd {

class Tank;

/****************************************************************************/

class PD_PUBLIC TankMedium
{
    friend class Tank;

    public:
        TankMedium(Tank *);
        ~TankMedium();

        /** Subscribe to a process variable.
         */
        void setLevelVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        /** Subscribe to a process variable.
         */
        void setLevelVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        /** Subscribe to a process variable.
         */
        void setVolumeVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        /** Subscribe to a process variable.
         */
        void setVolumeVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        void clearLevelVariable();
        void clearVolumeVariable();

        QColor getColor() const;
        void setColor(QColor);

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

class Q_DECL_EXPORT Tank:
    public QFrame
{
    friend class TankMedium;

    Q_OBJECT
    Q_ENUMS(LabelPosition Style)
    Q_PROPERTY(Style style
            READ getStyle WRITE setStyle RESET resetStyle)
    Q_PROPERTY(int labelWidth
            READ getLabelWidth WRITE setLabelWidth RESET resetLabelWidth)
    Q_PROPERTY(LabelPosition labelPosition
            READ getLabelPosition WRITE setLabelPosition
            RESET resetLabelPosition)
    Q_PROPERTY(int capHeight
            READ getCapHeight WRITE setCapHeight RESET resetCapHeight)
    Q_PROPERTY(double maxLevel
            READ getMaxLevel WRITE setMaxLevel RESET resetMaxLevel)
    Q_PROPERTY(double maxVolume
            READ getMaxVolume WRITE setMaxVolume RESET resetMaxVolume)
    Q_PROPERTY(int levelDecimals READ getLevelDecimals WRITE setLevelDecimals
            RESET resetLevelDecimals)
    Q_PROPERTY(int volumeDecimals READ getVolumeDecimals
            WRITE setVolumeDecimals RESET resetVolumeDecimals)
    Q_PROPERTY(QColor backgroundColor READ getBackgroundColor
            WRITE setBackgroundColor RESET resetBackgroundColor)
    Q_PROPERTY(QString levelSuffix READ getLevelSuffix
            WRITE setLevelSuffix RESET resetLevelSuffix)
    Q_PROPERTY(QString volumeSuffix READ getVolumeSuffix
            WRITE setVolumeSuffix RESET resetVolumeSuffix)

    public:
        Tank(QWidget * = 0);
        virtual ~Tank();

        QSize sizeHint() const;

        enum Style {
            VerticalCylinder,
            HorizontalCylinder,
            Cuboid
        };
        Style getStyle() const;
        void setStyle(Style);
        void resetStyle();

        int getLabelWidth() const;
        void setLabelWidth(int);
        void resetLabelWidth();

        enum LabelPosition {
            Right,
            Left
        };
        LabelPosition getLabelPosition() const;
        void setLabelPosition(LabelPosition);
        void resetLabelPosition();

        int getCapHeight() const;
        void setCapHeight(int);
        void resetCapHeight();

        double getMaxLevel() const;
        void setMaxLevel(double);
        void resetMaxLevel();

        double getMaxVolume() const;
        void setMaxVolume(double);
        void resetMaxVolume();

        int getLevelDecimals() const;
        void setLevelDecimals(int);
        void resetLevelDecimals();

        int getVolumeDecimals() const;
        void setVolumeDecimals(int);
        void resetVolumeDecimals();

        QColor getBackgroundColor() const;
        void setBackgroundColor(QColor);
        void resetBackgroundColor();

        QString getLevelSuffix() const;
        void setLevelSuffix(const QString &);
        void resetLevelSuffix();

        QString getVolumeSuffix() const;
        void setVolumeSuffix(const QString &);
        void resetVolumeSuffix();

        TankMedium *addMedium();
        void clearMedia();

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

        PD_PRIVATE bool event(QEvent *) override;
        PD_PRIVATE void resizeEvent(QResizeEvent *) override;
        PD_PRIVATE void paintEvent(QPaintEvent *) override;
};

} // namespace

/****************************************************************************/

#endif
