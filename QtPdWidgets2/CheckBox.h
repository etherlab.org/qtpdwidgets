/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_CHECKBOX_H
#define PD_CHECKBOX_H

#include <QCheckBox>
#include <QScopedPointer>

#include "Export.h"
#include <QtPdCom1/ScalarSubscriber.h>

namespace Pd {

class CheckBoxPrivate;

/****************************************************************************/

/** CheckBox.
 */
class PD_PUBLIC CheckBox:
    public QCheckBox, public QtPdCom::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(int onValue
            READ getOnValue WRITE setOnValue RESET resetOnValue)
    Q_PROPERTY(int offValue
            READ getOffValue WRITE setOffValue RESET resetOffValue)

    public:
        CheckBox(QWidget *parent = 0);
        virtual ~CheckBox();

        void clearData(); // pure-virtual from ScalarSubscriber

        int getOnValue() const;
        void setOnValue(int);
        void resetOnValue();
        int getOffValue() const;
        void setOffValue(int);
        void resetOffValue();

    protected:
        void checkStateSet();
        void nextCheckState();

    private:
        Q_DECLARE_PRIVATE(CheckBox);
        QScopedPointer<CheckBoxPrivate> const d_ptr;

        PD_PRIVATE void newValues(std::chrono::nanoseconds) override;
};

/****************************************************************************/

} // namespace

#endif
