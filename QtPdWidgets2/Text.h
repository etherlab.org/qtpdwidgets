/*****************************************************************************
 *
 * Copyright (C) 2009 - 2013  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_TEXT_H
#define PD_TEXT_H

#include <QFrame>
#include <QHash>
#include <QTimer>

#include "Export.h"
#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/ScalarVariable.h>

namespace Pd {

class Text;

/****************************************************************************/

/** Condition type.
 */
class PD_PUBLIC TextCondition:
    public QtPdCom::BoolVariable
{
    Q_OBJECT

    public:
        TextCondition(Text *);
        ~TextCondition();

        QString getText() const;
        void setText(const QString &);

        bool getInvert() const;
        void setInvert(bool);

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

/** Text display widget.
 *
 * \todo Default text property.
 */
class PD_PUBLIC Text:
    public QFrame, public QtPdCom::ScalarSubscriber
{
    Q_OBJECT

    friend class TextCondition;

    Q_PROPERTY(Qt::Alignment alignment
            READ getAlignment WRITE setAlignment RESET resetAlignment)
    Q_PROPERTY(QString prefix
            READ getPrefix WRITE setPrefix RESET resetPrefix)
    Q_PROPERTY(QString suffix
            READ getSuffix WRITE setSuffix RESET resetSuffix)
    Q_PROPERTY(bool conditionActive READ getConditionActive)

    public:
        Text(QWidget *parent = 0);
        virtual ~Text();

        Qt::Alignment getAlignment() const;
        void setAlignment(Qt::Alignment);
        void resetAlignment();

        QString getPrefix() const;
        void setPrefix(const QString &);
        void resetPrefix();

        QString getSuffix() const;
        void setSuffix(const QString &);
        void resetSuffix();

        void clearData(); // pure-virtual from ScalarSubscriber

        int getValue() const;
        void setValue(int);

        /** Value type.
         *
         * A tuple of text, text color and font, that can be provided for each
         * process value.
         */
        struct Value {
            QString text; /**< Displayed text. */
            QColor color; /**< Text color. */
            QFont font; /**< Font to use. */

            Value(
                    const QString &t = QString(), /**< Text. */
                    QColor c = QColor(), /**< Color. */
                    const QFont &f = QFont() /**< Font. */
                    ):
                text(t), color(c), font(f) {}
        };

        /** Value hash type.
         *
         * This hash shall contain a value object for each possible value to
         * display.
         */
        class Q_DECL_EXPORT Hash: public QHash<int, Value> {
            public:
                void insert(
                        int position,
                        const QString &t = QString(),
                        QColor c = QColor(),
                        const QFont &f = QFont());
        };
        void setHash(const Hash *);

        void updateValueText();

        TextCondition *addCondition(PdCom::Variable,
                const QString & = QString(), bool = false);
        TextCondition *addCondition(PdCom::Process *, const QString &,
                const QString & = QString(), bool = false);
        void clearConditions();

        bool getConditionActive() const;

    protected:
        bool event(QEvent *) override;
        void paintEvent(QPaintEvent *) override;

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

        PD_PRIVATE void newValues(std::chrono::nanoseconds) override;

    private slots:
        PD_PRIVATE void conditionChanged();
        PD_PRIVATE void conditionTimeout();
};

/****************************************************************************/

} // namespace

#endif
