/*****************************************************************************
 *
 * Copyright (C) 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_TABLEVIEW_H
#define PD_TABLEVIEW_H

#include <QTableView>
#include <QScopedPointer>

#include "Export.h"

namespace Pd {

class TableViewPrivate;

/****************************************************************************/

/** Table view widget.
 */
class PD_PUBLIC TableView:
    public QTableView
{
    Q_OBJECT

    public:
        TableView(QWidget *parent = 0);
        ~TableView();

    protected:
        bool event(QEvent *) override;
        void keyPressEvent(QKeyEvent *) override;
        void contextMenuEvent(QContextMenuEvent *) override;

    protected slots:
        void commit();
        void revert();
        void addRow();
        void removeRow();
        void exportCsv();
        void importCsv();
    private:
        QScopedPointer<TableViewPrivate> d_ptr;
        Q_DISABLE_COPY(TableView)

        Q_DECLARE_PRIVATE(TableView)
};

/****************************************************************************/

} // namespace Pd

#endif
