/*****************************************************************************
 *
 * Copyright (C) 2016  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_EXPORT_H
#define PD_EXPORT_H

#include <qglobal.h>

#define PD_PRIVATE Q_DECL_HIDDEN

#ifdef QtPdWidgets2_EXPORTS
#define PD_PUBLIC Q_DECL_EXPORT
#else
#define PD_PUBLIC Q_DECL_IMPORT
#endif  // QtPdWidgets2_EXPORTS

#endif  // PD_EXPORT_H
