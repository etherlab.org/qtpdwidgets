/*****************************************************************************
 *
 * Copyright (C) 2009 - 2023  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_IMAGE_H
#define PD_IMAGE_H

#include "Export.h"

#include <QtPdCom1/ScalarSubscriber.h>

#include <QFrame>
#include <QHash>

namespace Pd {

/****************************************************************************/

/** Image display widget.
 *
 * This widget can display images from a hash, depending on the attached
 * process variable's value.
 *
 * \todo Default image property.
 */
class PD_PUBLIC Image:
    public QFrame, public QtPdCom::ScalarSubscriber
{
    Q_OBJECT
    Q_PROPERTY(QPixmap defaultPixmap READ getDefaultPixmap
            WRITE setDefaultPixmap RESET resetDefaultPixmap)
    Q_PROPERTY(qreal angle READ getAngle WRITE setAngle RESET resetAngle)
    Q_PROPERTY(bool scaledContents READ hasScaledContents
            WRITE setScaledContents RESET resetScaledContents)

    public:
        Image(QWidget *parent = 0);
        virtual ~Image();

        void clearData(); // pure-virtual from ScalarSubscriber

        int getValue() const;
        void setValue(int);

        /** Pixmap hash type.
         *
         * The used pixmap hash shall contain a pixmap for every value to
         * display.
         */
        typedef QHash<int, QPixmap> PixmapHash;
        void setPixmapHash(const PixmapHash *);

        const QPixmap &getDefaultPixmap() const;
        void setDefaultPixmap(const QPixmap &);
        void resetDefaultPixmap();

        qreal getAngle() const;
        void setAngle(qreal);
        void resetAngle();

        bool hasScaledContents() const;
        void setScaledContents(bool);
        void resetScaledContents();

        void clearTransformations();
        void translate(qreal, qreal);
        enum Axis {X, Y};
        void translate(
                Axis axis, /**< Translation axis. */
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );
        void translate(
                Axis axis, /**< Translation axis. */
                PdCom::Process *p, /**< Process variable. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );
        void rotate(qreal);
        void rotate(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );
        void rotate(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const QtPdCom::Transmission & = QtPdCom::event_mode, /**< Transmission. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

    protected:
        bool event(QEvent *) override;
        void paintEvent(QPaintEvent *) override;

    private:
        struct PD_PRIVATE Impl;
        std::unique_ptr<Impl> impl;

        PD_PRIVATE void newValues(std::chrono::nanoseconds) override;
};

/****************************************************************************/

} // namespace

#endif
