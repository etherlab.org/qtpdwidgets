<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>Pd::Bar</name>
    <message>
        <location filename="src/Bar.cpp" line="872"/>
        <source>Bar Graph</source>
        <translation>Balkendiagramm</translation>
    </message>
</context>
<context>
    <name>Pd::Digital</name>
    <message>
        <location filename="src/Digital.cpp" line="472"/>
        <source>Digital display</source>
        <translation>Digitalanzeige</translation>
    </message>
</context>
<context>
    <name>Pd::Graph</name>
    <message>
        <location filename="src/Graph.cpp" line="772"/>
        <source>Graph</source>
        <translation>Kurve</translation>
    </message>
    <message>
        <location filename="src/Graph.cpp" line="773"/>
        <source>Run</source>
        <translation>Starten</translation>
    </message>
    <message>
        <location filename="src/Graph.cpp" line="774"/>
        <source>Stop</source>
        <translation>Anhalten</translation>
    </message>
</context>
<context>
    <name>Pd::Image</name>
    <message>
        <location filename="src/Image.cpp" line="78"/>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
</context>
<context>
    <name>Pd::Led</name>
    <message>
        <location filename="src/Led.cpp" line="90"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
</context>
<context>
    <name>Pd::MultiLed</name>
    <message>
        <location filename="src/MultiLed.cpp" line="77"/>
        <source>Multi-colored LED</source>
        <translation>Mehrfarbige LED</translation>
    </message>
</context>
<context>
    <name>Pd::NoPdTouchEdit</name>
    <message>
        <location filename="src/NoPdTouchEdit.cpp" line="310"/>
        <source>Touch entry</source>
        <translation>Touch-Editor</translation>
    </message>
</context>
<context>
    <name>Pd::TableView</name>
    <message>
        <location filename="src/TableView.cpp" line="204"/>
        <source>&amp;Commit</source>
        <translation>Ü&amp;bernehmen</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="206"/>
        <source>Commit edited data to process.</source>
        <translation>Schreibe die geänderten Daten zum Prozess.</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="207"/>
        <source>&amp;Revert</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="209"/>
        <source>Revert edited data.</source>
        <translation>Verwirft alle Änderungen.</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="211"/>
        <source>&amp;Add Row</source>
        <translation>&amp;Zeile hinzufügen</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="213"/>
        <source>Append a row to the table.</source>
        <translation>Fügt der Tabelle eine Zeile hinzu.</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="215"/>
        <source>&amp;Remove Row</source>
        <translation>Zeile &amp;entfernen</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="217"/>
        <source>Remove last row from table.</source>
        <translation>Entfernt eine Zeile aus der Tabelle.</translation>
    </message>
    <message>
        <source>Export as CSV</source>
        <translation>Als CSV exportieren</translation>
    </message>
    <message>
        <source>CSV Files (*.csv)</source>
        <translation>CSV-Dateien (*.csv)</translation>
    </message>
    <message>
        <source>Include column headers</source>
        <translation>Kopfzeile der Spalten übernehmen</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation>Export fehlgeschlagen</translation>
    </message>
    <message>
        <source>Creating CSV file failed.</source>
        <translation>Erstellen der CSV-Datei fehlgeschlagen.</translation>
    </message>
    <message>
        <source>Export as CSV...</source>
        <translation>Als CSV exportieren...</translation>
    </message>
    <message>
        <source>Export table data as csv file.</source>
        <translation>Daten als CSV-Datei exportieren</translation>
    </message>
    <message>
        <source>CSV headers:</source>
        <translation>CSV Kopfzeile:</translation>
    </message>
    <message>
        <source>CSV separator:</source>
        <translation>CSV Spaltentrennzeichen:</translation>
    </message>
    <message>
        <source>Comma (,)</source>
        <translation>Komma (,)</translation>
    </message>
    <message>
        <source>Semicolon (;)</source>
        <translation>Semikolon (;)</translation>
    </message>
    <message>
        <source>Import from CSV</source>
        <translation>CSV Import</translation>
    </message>
    <message>
        <source>First Row (zero based):</source>
        <translation>Erste Zeile (von 0 beginnend):</translation>
    </message>
    <message>
        <source>First Column (zero based):</source>
        <translation>Erste Spalte (von 0 beginnend):</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation>Datenimport fehlgeschlagen</translation>
    </message>
    <message>
        <source>Reading CSV file failed.</source>
        <translation>Einlesen der CSV-Datei fehlgeschlagen.</translation>
    </message>
    <message>
        <source>CSV import</source>
        <translation>CSV Import</translation>
    </message>
    <message>
        <source>Failed to import CSV file.</source>
        <translation>Import der CSV-Datei fehlgeschlagen.</translation>
    </message>
    <message>
        <source>Imported %1 rows.</source>
        <translation>%1 Zeilen importiert.</translation>
    </message>
    <message>
        <source>Import from CSV...</source>
        <translation>CSV-Datei importieren...</translation>
    </message>
    <message>
        <source>Import table data from a csv file.</source>
        <translation>Daten aus einer CSV-Datei einlesen.</translation>
    </message>
    <message>
        <source>Pipe (|)</source>
        <translation>Senkrechter Strich (|)</translation>
    </message>
</context>
<context>
    <name>Pd::Text</name>
    <message>
        <location filename="src/Text.cpp" line="221"/>
        <source>Text</source>
        <translation>Textanzeige</translation>
    </message>
</context>
<context>
    <name>Pd::Time</name>
    <message>
        <location filename="src/Time.cpp" line="103"/>
        <source>Time display</source>
        <translation>Zeitanzeige</translation>
    </message>
</context>
<context>
    <name>Pd::TouchEdit</name>
    <message>
        <location filename="src/TouchEdit.cpp" line="101"/>
        <source>Digital display and touch entry</source>
        <translation>Digitalanzeige mit Touch-Editor</translation>
    </message>
</context>
<context>
    <name>Pd::TouchEditDialog</name>
    <message>
        <location filename="src/TouchEditDialog.cpp" line="108"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="src/TouchEditDialog.cpp" line="109"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="src/TouchEditDialog.cpp" line="110"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Pd::XYGraph</name>
    <message>
        <location filename="src/XYGraph.cpp" line="749"/>
        <source>XY Graph</source>
        <translation>XY-Diagramm</translation>
    </message>
</context>
<context>
    <name>SendBroadcastWidgetPrivate</name>
    <message>
        <location filename="src/SendBroadcastWidget.cpp" line="43"/>
        <source>Send</source>
        <translation>Senden</translation>
    </message>
</context>
</TS>
