<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL" sourcelanguage="en">
<context>
    <name>Pd::Bar</name>
    <message>
        <location filename="src/Bar.cpp" line="872"/>
        <source>Bar Graph</source>
        <translation>Staafdiagram</translation>
    </message>
</context>
<context>
    <name>Pd::Digital</name>
    <message>
        <location filename="src/Digital.cpp" line="472"/>
        <source>Digital display</source>
        <translation>Digitaal display</translation>
    </message>
</context>
<context>
    <name>Pd::Graph</name>
    <message>
        <location filename="src/Graph.cpp" line="772"/>
        <source>Graph</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <location filename="src/Graph.cpp" line="773"/>
        <source>Run</source>
        <translation>Lopen</translation>
    </message>
    <message>
        <location filename="src/Graph.cpp" line="774"/>
        <source>Stop</source>
        <translation>Stilhouden</translation>
    </message>
</context>
<context>
    <name>Pd::Image</name>
    <message>
        <location filename="src/Image.cpp" line="78"/>
        <source>Image</source>
        <translation>Beeld</translation>
    </message>
</context>
<context>
    <name>Pd::Led</name>
    <message>
        <location filename="src/Led.cpp" line="90"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
</context>
<context>
    <name>Pd::MultiLed</name>
    <message>
        <location filename="src/MultiLed.cpp" line="77"/>
        <source>Multi-colored LED</source>
        <translation>Meerkleurige LED</translation>
    </message>
</context>
<context>
    <name>Pd::NoPdTouchEdit</name>
    <message>
        <location filename="src/NoPdTouchEdit.cpp" line="310"/>
        <source>Touch entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Pd::TableView</name>
    <message>
        <location filename="src/TableView.cpp" line="204"/>
        <source>&amp;Commit</source>
        <translation>&amp;Aannemen</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="206"/>
        <source>Commit edited data to process.</source>
        <translation>Gewijzigde data aannemen.</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="207"/>
        <source>&amp;Revert</source>
        <translation>&amp;Terugdraaien</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="209"/>
        <source>Revert edited data.</source>
        <translation>Gewijzigde data terugdraaien.</translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="211"/>
        <source>&amp;Add Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="213"/>
        <source>Append a row to the table.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="215"/>
        <source>&amp;Remove Row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/TableView.cpp" line="217"/>
        <source>Remove last row from table.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Pd::Text</name>
    <message>
        <location filename="src/Text.cpp" line="221"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
</context>
<context>
    <name>Pd::Time</name>
    <message>
        <location filename="src/Time.cpp" line="103"/>
        <source>Time display</source>
        <translation>Tijd display</translation>
    </message>
</context>
<context>
    <name>Pd::TouchEdit</name>
    <message>
        <location filename="src/TouchEdit.cpp" line="101"/>
        <source>Digital display and touch entry</source>
        <translation>Digitale display met aanraakscherm</translation>
    </message>
</context>
<context>
    <name>Pd::TouchEditDialog</name>
    <message>
        <location filename="src/TouchEditDialog.cpp" line="108"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="src/TouchEditDialog.cpp" line="109"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="src/TouchEditDialog.cpp" line="110"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>Pd::XYGraph</name>
    <message>
        <location filename="src/XYGraph.cpp" line="749"/>
        <source>XY Graph</source>
        <translation>XY Diagram</translation>
    </message>
</context>
<context>
    <name>SendBroadcastWidgetPrivate</name>
    <message>
        <location filename="src/SendBroadcastWidget.cpp" line="43"/>
        <source>Send</source>
        <translation>Verzenden</translation>
    </message>
</context>
</TS>
