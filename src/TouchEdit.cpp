/*****************************************************************************
 *
 * Copyright (C)        2013  Florian Pose <fp@igh.de>
 *               2011 - 2012  Andreas Stewering-Bone <ab@igh-essen.com>
 *                            Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TouchEdit.h"
using Pd::TouchEdit;

#include "TouchEditDialog.h"

#include <QtGui>
#include <QDebug>
#include <QStyle>
#include <limits>

/****************************************************************************/

#define DEFAULT_LOWERLIMIT  (-std::numeric_limits<double>::infinity())
#define DEFAULT_UPPERLIMIT  (std::numeric_limits<double>::infinity())

/****************************************************************************/

struct TouchEdit::Impl
{
    Impl(TouchEdit *parent):
        parent{parent},
        lowerLimit(DEFAULT_LOWERLIMIT),
        upperLimit(DEFAULT_UPPERLIMIT),
        editDigit(0),
        editing(false),
        editDialog(NULL)
    {
    }

    TouchEdit * const parent;

    double lowerLimit;
    double upperLimit;
    int editDigit;
    bool editing;
    TouchEditDialog *editDialog;

    /** Opens the edit dialog.
     */
    void openDialog()
    {
        if (!parent->hasVariable() || !parent->isEnabled()) {
            return;
        }

        if (!editDialog) { // create the dialog
            editDialog = new TouchEditDialog(parent);
        }

        editDialog->setValue(parent->getValue());
        editDialog->setLowerLimit(lowerLimit);
        editDialog->setUpperLimit(upperLimit);
        editDialog->setSuffix(parent->getSuffix());
        editDialog->setDecimals(parent->getDecimals());
        editDialog->setEditDigit(editDigit);

        editing = true;
        parent->style()->unpolish(parent);
        parent->style()->polish(parent);

        if (editDialog->exec()) {
            parent->writeValue(editDialog->getValue());
            editDigit = editDialog->getEditDigit(); // remember last edited
                                                    // digit
        }

        editing = false;
        parent->style()->unpolish(parent);
        parent->style()->polish(parent);
    }

    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(
                Pd::TouchEdit::tr("Digital display and touch entry"));
    }

};

/****************************************************************************/

/** Constructor.
 */
TouchEdit::TouchEdit(
        QWidget *parent /**< Parent widget. */
        ): Digital(parent),
    impl{std::unique_ptr<Impl>{new Impl(this)}}
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
TouchEdit::~TouchEdit()
{
}

/****************************************************************************/

/**
 * \return The lowerLimit.
 */
double TouchEdit::getLowerLimit() const
{
    return impl->lowerLimit;
}

/****************************************************************************/

/** Sets the lowerLimit.
 */
void TouchEdit::setLowerLimit(double limit)
{
    if (limit != impl->lowerLimit) {
        impl->lowerLimit = limit;
    }
}

/****************************************************************************/

/** Resets the lowerLimit.
 */
void TouchEdit::resetLowerLimit()
{
    setLowerLimit(DEFAULT_LOWERLIMIT);
}

/****************************************************************************/

/**
 * \return The upperLimit.
 */
double TouchEdit::getUpperLimit() const
{
    return impl->upperLimit;
}

/****************************************************************************/

/** Sets the upperLimit.
 */
void TouchEdit::setUpperLimit(double limit)
{
    if (limit != impl->upperLimit) {
        impl->upperLimit = limit;
    }
}

/****************************************************************************/

/** Resets the upperLimit.
 */
void TouchEdit::resetUpperLimit()
{
    setUpperLimit(DEFAULT_UPPERLIMIT);
}

/****************************************************************************/

bool TouchEdit::getEditing() const
{
    return impl->editing;
}

/****************************************************************************/

/** Eventhandler.
 */
bool TouchEdit::event(QEvent *event)
{
    switch (event->type()) {
        case QEvent::MouseButtonPress:
            impl->openDialog();
            return true;

        case QEvent::LanguageChange:
            impl->retranslate();
            break;

        default:
            break;
    }

    return Digital::event(event);
}

/****************************************************************************/

/** React to state changes.
 */
void TouchEdit::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::EnabledChange) {
        if (!isEnabled() && impl->editDialog &&
                impl->editDialog->isVisible()) {
            /* close the editDialog if it is open
             * while we receive a disable. */
            impl->editDialog->done(QDialog::Rejected);
        }

        update();
    }

    Digital::changeEvent(event);
}

/****************************************************************************/
