/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <QtPdWidgets2.h>

#include "Widget.h"

#include <git_revision_hash.h>


#define STR(x) #x
#define QUOTE(x) STR(x)


const char* const QtPdWidgets2::qtpdwidgets_version_code = QUOTE(QTPDWIDGETS_MAJOR) "." QUOTE(QTPDWIDGETS_MINOR) "." QUOTE(QTPDWIDGETS_RELEASE);

const char* const QtPdWidgets2::qtpdwidgets_full_version = GIT_REV;

void QtPdWidgets2::setRedrawInterval(int i)
{
    Pd::Widget::setRedrawInterval(i);
}
