/*****************************************************************************
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *               2023 Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ParameterFileSystemModel.h"

#include <algorithm>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFileIconProvider>
#include <QBrush>
#include <QTimer>

using Pd::ParameterFileSystemModel;

/*****************************************************************************
 * public
 ****************************************************************************/

ParameterFileSystemModel::ParameterFileSystemModel(QObject *parent):
    QAbstractListModel(parent)
{
}


constexpr std::chrono::seconds ParameterFileSystemModel::DELETE_DELAY;

/****************************************************************************/

ParameterFileSystemModel::~ParameterFileSystemModel()
{
}

QFileInfoList ParameterFileSystemModel::getInfo() const
{
    // ordering is important for difference() below.
    return QDir(path_).entryInfoList(QDir::Files, QDir::Name);
}

void ParameterFileSystemModel::addItems(const QFileInfoList &items)
{
    auto it = files.begin();
    for (const auto& item : items) {
        // forward behind position to insert
        while (it != files.end() && item.fileName() >= it->fileName()) {
            ++it;
        }
        const int row = it - files.begin();
        beginInsertRows({}, row, row);
        it = files.insert(it, item);
        endInsertRows();
    }
}

/****************************************************************************/

void ParameterFileSystemModel::setPath(const QString path)
{
    beginResetModel();
    path_ = path;
    files = getInfo();
    endResetModel();
}

/****************************************************************************/

int ParameterFileSystemModel::rowCount(const QModelIndex &parent) const
{
    int ret = 0;

    if (parent.isValid()) {
        qWarning() << __func__ << parent << "inval";
    } else {
        ret = files.size();
    }

#ifdef DEBUG_FILESYSTEM
    qDebug() << __func__ << parent << ret;
#endif

    return ret;
}

/****************************************************************************/

QVariant ParameterFileSystemModel::data(const QModelIndex &index,
                                        int role) const {
    QVariant ret;

    if (index.isValid()) {
        QFileInfo info = files.at(index.row());

        switch (role) {
            case Qt::DisplayRole: {
                ret = info.fileName();
            } break;
            case Qt::DecorationRole: {
                QFileIconProvider iconP;
                ret = iconP.icon(info);
            } break;
            default:
                break;
        }
    }

#ifdef DEBUG_FILESYSTEM
    if (role <= 1) {
        qDebug() << __func__ << index << role << ret;
    }
#endif

    return ret;
}

/****************************************************************************/

Qt::ItemFlags ParameterFileSystemModel::flags(const QModelIndex &idx) const
{
    auto ans = QAbstractListModel::flags(idx);
    // disable item if file is deleted
    if (idx.isValid() && idx.row() < files.size() && files.at(idx.row()).is_really_deleted_)
    {
        ans = ans & ~(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    }
    return ans;
}

/****************************************************************************/

void Pd::ParameterFileSystemModel::removeReallyDeletedFiles()
{
    for (auto it = files.begin(); it != files.end();) {
        if (it->is_really_deleted_) {
            const int row = it - files.begin();
            beginRemoveRows({}, row, row);
            it = files.erase(it);
            endRemoveRows();
        } else {
            ++it;
        }
    }
}

/****************************************************************************/

template<class F1, class F2>
static QFileInfoList difference(const F1& fl1, const F2&fl2)
{
    const auto cmp = [](typename F1::const_reference f1, typename F2::const_reference f2) {
        return f1.fileName() < f2.fileName();
    };
    QFileInfoList ans;
    std::set_difference(fl1.begin(), fl1.end(), fl2.begin(), fl2.end(),
            std::back_inserter<QFileInfoList>(ans), cmp);
    return ans;
}

/****************************************************************************/


template<class F1, class F2>
static bool compareFileInfo(const F1& f1, const F2&f2) {
    if (f1.lastModified() != f2.lastModified())
        return false;
    if (f1.metadataChangeTime() != f2.metadataChangeTime())
        return false;
    return true;
}

void ParameterFileSystemModel::refresh()
{
    const auto newList = getInfo();
#ifdef DEBUG_FILESYSTEM
    qDebug() << "newList:" << newList << ", oldList:" << files;
#endif

    addItems(difference(newList, files));
    // files now has everything in newList
    auto files_it = files.begin();
    for (const auto& new_item : newList) {
        while (files_it->fileName() != new_item.fileName()) {
            // process removed files
            startDeleteDelayTimer(index(files_it - files.begin()));
            ++files_it;
            Q_ASSERT(files_it != files.end());
        }
        if (!compareFileInfo(*files_it, new_item)) {
            const auto idx = index(files_it - files.begin());
            *files_it = new_item;
            emit dataChanged(idx, idx);
        }
        ++files_it;
    }
    // process remaining removed files
    for (; files_it != files.end(); ++files_it) {
        startDeleteDelayTimer(index(files_it - files.begin()));
    }

}

void ParameterFileSystemModel::startDeleteDelayTimer(const QPersistentModelIndex& idx)
{
    Q_ASSERT(idx.isValid());
    auto& item = files[idx.row()];
    if (item.delete_delay_timer_running_)
        return;

    item.delete_delay_timer_running_ = true;
    QTimer::singleShot(DELETE_DELAY, this, [this,idx] () {
        if (!idx.isValid() || idx.row() >= files.size())
            return;
        auto& item = files[idx.row()];
        item.delete_delay_timer_running_ = false;
        item.is_really_deleted_ = true;
        emit dataChanged(idx, idx);
    });
}

/****************************************************************************/
