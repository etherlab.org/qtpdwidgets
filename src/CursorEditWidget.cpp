/*****************************************************************************
 *
 * Copyright (C) 2011  Andreas Stewering-Bone <ab@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <math.h>

#include <iomanip>
#include <limits>

#include <QtGui>

#include "CursorEditWidget.h"

#ifndef MAX
# define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif
#ifndef ABS
# define ABS(x) ((x) > 0.0 ? (x) : -(x))
#endif
#define CURSOR_INF  (std::numeric_limits<double>::infinity())

namespace Pd {
class CursorEditWidgetPrivate {

    CursorEditWidgetPrivate(CursorEditWidget *parent) : q_ptr(parent) {}

    CursorEditWidget *const q_ptr;
    double value = 0.0;
    int decimals = 0;
    QString suffix;
    double lowerLimit = -CURSOR_INF;
    double upperLimit = CURSOR_INF;
    int digPos        = 0;
    QString valueStr;

    void updateValueStr();

    Q_DECLARE_PUBLIC(CursorEditWidget)
};
}

using Pd::CursorEditWidget;
using Pd::CursorEditWidgetPrivate;

/****************************************************************************/

/** Constructor.
 */
CursorEditWidget::CursorEditWidget(
        QWidget *parent /**< parent widget */
        ):
    QWidget(parent),
    d_ptr(new CursorEditWidgetPrivate(this))
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    updateGeometry();
}

/****************************************************************************/

/** Destructor.
 */
CursorEditWidget::~CursorEditWidget()
{
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize CursorEditWidget::sizeHint() const
{
    return QSize(300, 100);
}

/****************************************************************************/

/** Sets actual value.
 */
void CursorEditWidget::setValue(double v)
{
    Q_D(CursorEditWidget);
    bool valueChanged = false;

    if (v != d->value) {
        d->value = v;
        d->digPos = 0;
        valueChanged = true;
    }

    if (valueChanged || d->valueStr.isEmpty()) {
        d->updateValueStr();
    }
}

/****************************************************************************/

/** Set editable decimals.
 */
void CursorEditWidget::setDecimals(int dd)
{
    Q_D(CursorEditWidget);
    bool valueChanged = false;

    if (dd < 0) {
        dd = 0;
    }

    if (dd != d->decimals) {
        d->decimals = dd;
        d->digPos = 0;
        valueChanged = true;
    }

    if (valueChanged || d->valueStr.isEmpty()) {
        d->updateValueStr();
    }
}

/****************************************************************************/

/** Sets suffix.
 */
void CursorEditWidget::setSuffix(const QString &s)
{
    Q_D(CursorEditWidget);
    if (s != d->suffix) {
        d->suffix = s;
        update();
    }
}

/****************************************************************************/

/** Sets lower limit.
 */
void CursorEditWidget::setLowerLimit(double limit)
{
    Q_D(CursorEditWidget);
    if (limit != d->lowerLimit) {
        d->lowerLimit = limit;

        if (d->value < d->lowerLimit) {
            setValue(d->lowerLimit);
        }
    }
}

/****************************************************************************/

/** Sets upper limit.
 */
void CursorEditWidget::setUpperLimit(double limit)
{
    Q_D(CursorEditWidget);
    if (limit != d->upperLimit) {
        d->upperLimit = limit;

        if (d->value > d->upperLimit) {
            setValue(d->upperLimit);
        }
    }
}

/****************************************************************************/

/** Sets cursor position.
 */
void CursorEditWidget::setEditDigit(int dig)
{
    Q_D(CursorEditWidget);
    if (dig < -d->decimals) {
        dig = -d->decimals;
    }

    if (d->upperLimit != CURSOR_INF && d->lowerLimit != -CURSOR_INF) {
        double emax = MAX(
                floor(log10(ABS(d->upperLimit))),
                floor(log10(ABS(d->lowerLimit))));
        if (dig > emax) {
            dig = emax;
        }
    }

    if (dig != d->digPos) {
        d->digPos = dig;
        d->updateValueStr();
    }
}

/****************************************************************************/

/** Moves cursor one digit left.
 */
void CursorEditWidget::digitLeft()
{
    Q_D(CursorEditWidget);
    setEditDigit(d->digPos + 1);
}

/****************************************************************************/

/** Moves cursor one digit right.
 */
void CursorEditWidget::digitRight()
{
    Q_D(CursorEditWidget);
    setEditDigit(d->digPos - 1);
}

/****************************************************************************/

/** Increment actual digit.
 */
void CursorEditWidget::digitUp()
{
    Q_D(CursorEditWidget);
    double digitValue = pow(10, d->digPos);
    double eps = 0.5 * pow(10, -d->decimals - d->digPos);
    double r = floor(d->value / digitValue + eps) * digitValue;
    d->value = r + digitValue;
    if (d->value > d->upperLimit) {
        d->value = d->upperLimit;
    }
    d->updateValueStr();
}

/****************************************************************************/

/** Decrement actual digit.
 */
void CursorEditWidget::digitDown()
{
    Q_D(CursorEditWidget);
    double digitValue = pow(10, d->digPos);
    double eps = 0.5 * pow(10, -d->decimals - d->digPos);
    double r = ceil(d->value / digitValue - eps) * digitValue;
    d->value = r - digitValue;
    if (d->value < d->lowerLimit) {
        d->value = d->lowerLimit;
    }
    d->updateValueStr();
}

/****************************************************************************/

/** Reset Value.
 */
void CursorEditWidget::setZero()
{
    Q_D(CursorEditWidget);
    if (d->lowerLimit > 0.0) {
        d->value = d->lowerLimit;
    }
    else if (d->upperLimit < 0.0) {
        d->value = d->upperLimit;
    }
    else {
        d->value = 0.0;
    }

    d->updateValueStr();
}

/****************************************************************************/

/** Paint function.
 */
void CursorEditWidget::paintEvent(
        QPaintEvent *event /**< paint event flags */
        )
{
    Q_D(CursorEditWidget);
    QPainter painter(this);
    const auto &valueStr(d->valueStr);

    painter.setRenderHint(QPainter::Antialiasing);

    // draw background
    painter.fillRect(event->rect(), Qt::gray);

    if (!valueStr.isEmpty()) {
        int pos, digCount = 0;
        QString html;

        for (pos = valueStr.size() - 1; pos >= 0; pos--) {
            if (valueStr[pos].isNumber()) {
                if (d->digPos + d->decimals == digCount) {
                    html = QStringLiteral("<span style=\""
                        "color: blue; "
                        "text-decoration: underline;"
                        "\">%1</span>%2").arg(valueStr[pos]).arg(html);
                } else {
                    html = valueStr[pos] + html;
                }

                digCount++;
            } else {
                html = valueStr[pos] + html;
            }
        }

        html = "<html><head><meta http-equiv=\"Content-Type\" "
            "content=\"text/html; charset=utf-8\"></head><body>"
            "<div align=\"center\" style=\""
            "color: white; "
            "font-size: 24pt;"
            "\">" + html + d->suffix + "</div></body></html>";

        QTextDocument doc;
        doc.setPageSize(rect().size());
        doc.setHtml(html);
        painter.translate(0.0, (rect().height() - doc.size().height()) / 2);
        doc.drawContents(&painter, rect());
    }
}

/****************************************************************************/

/** convert value to displayed string.
 */
void CursorEditWidgetPrivate::updateValueStr()
{
    Q_Q(CursorEditWidget);
    QString fmt;
    unsigned int width = digPos + decimals + 1;

    if (decimals > 0) {
        width++; // with dot
    }

    /* In Qt4, the zero padding worked without passing the QLatin1Char('0')
     * parameter. In Qt5, the parameter is necessary, but does not give valid
     * results for negative numbers. Thus pretend we are positive and add the
     * minus sign afterwards.
     */
    valueStr = QString("%1").arg(fabs(value), width, 'f', decimals,
            QLatin1Char('0'));
    if (value < 0.0) {
        valueStr = "-" + valueStr;
    }

    q->update();
}

/****************************************************************************/

/**
 * \return The value.
 */
double CursorEditWidget::getValue() const
{
    const Q_D(CursorEditWidget);
    return d->valueStr.toDouble();
}

/****************************************************************************/

/**
 * \return The digPos.
 */
int CursorEditWidget::getEditDigit() const
{
    const Q_D(CursorEditWidget);
    return d->digPos;
}
