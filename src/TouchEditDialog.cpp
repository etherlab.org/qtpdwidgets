/*****************************************************************************
 *
 * Copyright (C) 2011  Andreas Stewering-Bone <ab@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TouchEditDialog.h"


#include <QVBoxLayout>
#include <QPushButton>


namespace Pd {
class TouchEditDialogPrivate {
public:
    TouchEditDialogPrivate(TouchEditDialog *parent);

    CursorEditWidget editWidget;
    QVBoxLayout vboxLayout;
    QGridLayout gLayout;
    QPushButton buttonLeft;
    QPushButton buttonRight;
    QPushButton buttonUp;
    QPushButton buttonDown;
    QPushButton buttonOk;
    QPushButton buttonZero;
    QPushButton buttonCancel;

    void retranslate();
};
}
using Pd::TouchEditDialog;
using Pd::TouchEditDialogPrivate;

#include <QtGui>
#include <QDebug>

/****************************************************************************/

/** Constructor.
 */
TouchEditDialog::TouchEditDialog(
        QWidget *parent /**< parent widget */
        ):
    QDialog(parent),
    d_ptr(new TouchEditDialogPrivate(this))
{
    setModal(true);
    setResult(QDialog::Rejected);
    resize(300, 200);

}

TouchEditDialogPrivate::TouchEditDialogPrivate(TouchEditDialog *parent) {
    vboxLayout.addWidget(&editWidget);
    vboxLayout.setSpacing(0);
    vboxLayout.setContentsMargins(0, 0, 0, 0);
    parent->setLayout(&vboxLayout);

    buttonLeft.setIcon(QIcon(":/QtPdWidgets/images/go-previous.png"));
    buttonLeft.setIconSize(QSize(32, 32));
    buttonLeft.setMinimumHeight(50);

    buttonRight.setIcon(QIcon(":/QtPdWidgets/images/go-next.png"));
    buttonRight.setIconSize(QSize(32, 32));
    buttonRight.setMinimumHeight(50);

    buttonUp.setIcon(QIcon(":/QtPdWidgets/images/go-up.png"));
    buttonUp.setIconSize(QSize(32, 32));
    buttonUp.setMinimumHeight(50);

    buttonDown.setIcon(QIcon(":/QtPdWidgets/images/go-down.png"));
    buttonDown.setIconSize(QSize(32, 32));
    buttonDown.setMinimumHeight(50);

    buttonOk.setMinimumHeight(50);

    buttonZero.setMinimumHeight(50);

    buttonCancel.setMinimumHeight(50);

    gLayout.addWidget(&buttonUp, 0, 1);
    gLayout.addWidget(&buttonLeft, 1, 0);
    gLayout.addWidget(&buttonZero, 1, 1);
    gLayout.addWidget(&buttonRight, 1, 2);
    gLayout.addWidget(&buttonCancel, 2, 0);
    gLayout.addWidget(&buttonDown, 2, 1);
    gLayout.addWidget(&buttonOk, 2, 2);
    vboxLayout.addLayout(&gLayout);

    retranslate();

    QObject::connect(&buttonLeft, SIGNAL(clicked()), &editWidget, SLOT(digitLeft()));
    QObject::connect(&buttonRight, SIGNAL(clicked()), &editWidget, SLOT(digitRight()));
    QObject::connect(&buttonUp, SIGNAL(clicked()), &editWidget, SLOT(digitUp()));
    QObject::connect(&buttonDown, SIGNAL(clicked()), &editWidget, SLOT(digitDown()));
    QObject::connect(&buttonZero, SIGNAL(clicked()), &editWidget, SLOT(setZero()));

    QObject::connect(&buttonOk, SIGNAL(clicked()), parent, SLOT(buttonOk_clicked()));
    QObject::connect(&buttonCancel, SIGNAL(clicked()),
                     parent, SLOT(buttonCancel_clicked()));
}

/****************************************************************************/

void TouchEditDialog::changeEvent(QEvent* event)
{
    if (event && event->type() == QEvent::LanguageChange)
    {
        retranslate();
    }

    // remember to call base class implementation
    QDialog::changeEvent(event);
}

/****************************************************************************/

void TouchEditDialog::retranslate()
{
    Q_D(TouchEditDialog);
    d->retranslate();
}

void TouchEditDialogPrivate::retranslate()
{
    buttonOk.setText(Pd::TouchEditDialog::tr("Ok"));
    buttonZero.setText(Pd::TouchEditDialog::tr("0"));
    buttonCancel.setText(Pd::TouchEditDialog::tr("Cancel"));
}

/****************************************************************************/

/** Destructor.
 */
TouchEditDialog::~TouchEditDialog()
{
}

/****************************************************************************/

/** Accept Value.
 */
void TouchEditDialog::buttonOk_clicked()
{
    // if this dialog is used in an item delegate, it is important, that
    // result is set, before close() is called. thus two calls.
    setResult(QDialog::Accepted);
    done(QDialog::Accepted);
}

/****************************************************************************/

/** Abort Dialog.
 */
void TouchEditDialog::buttonCancel_clicked()
{
    reject();
}

/****************************************************************************/

/** Set the edit widget value.
 */
void TouchEditDialog::setValue(double value)
{
    Q_D(TouchEditDialog);
    return d->editWidget.setValue(value);
}

/****************************************************************************/

/** Set the edit widget decimals.
 */
void TouchEditDialog::setDecimals(int value)
{
    Q_D(TouchEditDialog);
    d->editWidget.setDecimals(value);
}

/****************************************************************************/

/** Set the edit widget suffix.
 */
void TouchEditDialog::setSuffix(const QString &value)
{
    Q_D(TouchEditDialog);
    d->editWidget.setSuffix(value);
}

/****************************************************************************/

/** Set the edit widget lower limit.
 */
void TouchEditDialog::setLowerLimit(double value)
{
    Q_D(TouchEditDialog);
    d->editWidget.setLowerLimit(value);
}

/****************************************************************************/

/** Set the edit widget upper limit.
 */
void TouchEditDialog::setUpperLimit(double value)
{
    Q_D(TouchEditDialog);
    d->editWidget.setUpperLimit(value);
}

/****************************************************************************/

/** Set the edit widget cursor position.
 */
void TouchEditDialog::setEditDigit(int value)
{
    Q_D(TouchEditDialog);
    d->editWidget.setEditDigit(value);
}

/****************************************************************************/

double TouchEditDialog::getValue() const
{
    const Q_D(TouchEditDialog);
    return d->editWidget.getValue();
}

int TouchEditDialog::getEditDigit() const
{
    const Q_D(TouchEditDialog);
    return d->editWidget.getEditDigit();
};
