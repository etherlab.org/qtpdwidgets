/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <QtGui>

#include "CheckBox.h"

#define DEFAULT_ONVALUE 1
#define DEFAULT_OFFVALUE 0

namespace Pd {
class CheckBoxPrivate {
    CheckBoxPrivate(CheckBox *parent) : q_ptr(parent) {}

    int value = 0; /**< the current variable value */
    bool dataPresent = false; /**< A value is to be displayed. */
    int onValue = DEFAULT_ONVALUE; /**< The value that is sent to the process, if the
                    checkbox is checked. */
    int offValue = DEFAULT_OFFVALUE; /**< The value that is sent to the process, if the
                    checkbox is unchecked. */

    void setYellow(bool);
    void updateCheck();

    Q_DECLARE_PUBLIC(CheckBox);
    CheckBox *const q_ptr;
};
}

using Pd::CheckBox;
using Pd::CheckBoxPrivate;

/****************************************************************************/


/****************************************************************************/

/** Constructor.
 */
CheckBox::CheckBox(
        QWidget *parent /**< parent widget */
        ): QCheckBox(parent),
        d_ptr(new CheckBoxPrivate(this))
{
}

/****************************************************************************/

/** Destructor.
 */
CheckBox::~CheckBox()
{
}

/****************************************************************************/

void CheckBox::clearData()
{
    Q_D(CheckBox);
    d->dataPresent = false;
    d->updateCheck();
}

/****************************************************************************/

/** Sets the #onValue.
 */
void CheckBox::setOnValue(int v)
{
    Q_D(CheckBox);
    if (v != d->onValue) {
        d->onValue = v;
        d->updateCheck();
    }
}

/****************************************************************************/

/** Resets the #onValue.
 */
void CheckBox::resetOnValue()
{
    setOnValue(DEFAULT_ONVALUE);
}

/****************************************************************************/

/** Sets the #offValue.
 */
void CheckBox::setOffValue(int v)
{
    Q_D(CheckBox);
    if (v != d->offValue) {
        d->offValue = v;
        d->updateCheck();
    }
}

/****************************************************************************/

/** Resets the #offValue.
 */
void CheckBox::resetOffValue()
{
    setOffValue(DEFAULT_OFFVALUE);
}

/****************************************************************************/

/** From QAbstractButton.
 *
 * \todo Check, if this has to be implemented.
 */
void CheckBox::checkStateSet()
{
}

/****************************************************************************/

/** Sets the next check state.
 *
 * This virtual method originating from QAbstractButton is called, when the
 * user requests a new check state (by clicking on the check box, or by
 * hitting the acceleration key, etc.). It is designed to let subclasses
 * implement intermediate check states.
 *
 * Here it is used to write the new state to the process first and to delay
 * the setting of the new check state until the process variable was written.
 */
void CheckBox::nextCheckState()
{
    Q_D(CheckBox);
    if (checkState() == Qt::Unchecked) {
        writeValue(d->onValue);
        d->setYellow(true);
    } else {
        writeValue(d->offValue);
        d->setYellow(true);
    }
}

/****************************************************************************/

/** Makes the background yellow or white again.
 */
void CheckBoxPrivate::setYellow(bool y)
{
    Q_Q(CheckBox);
    QPalette p = q->palette();
    p.setColor(QPalette::Base, y ? Qt::yellow : Qt::white);
    q->setPalette(p);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void CheckBox::newValues(std::chrono::nanoseconds)
{
    Q_D(CheckBox);
    int32_t newValue;
    PdCom::details::copyData(&newValue,
            PdCom::details::TypeInfoTraits<int32_t>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    newValue = newValue * scale + offset;

    d->setYellow(false);

    if (newValue != d->value || !d->dataPresent) {
        d->value = newValue;
        d->dataPresent = true;
        d->updateCheck();
    }
}

/****************************************************************************/

/** Updates the displayed value.
 */
void CheckBoxPrivate::updateCheck()
{
    Q_Q(CheckBox);
    if (dataPresent && value == onValue) {
        q->setCheckState(Qt::Checked);
    } else if (dataPresent && value != offValue) {
        q->setCheckState(Qt::PartiallyChecked);
    } else {
        q->setCheckState(Qt::Unchecked);
    }
}

/****************************************************************************/

/** \returns The #onValue.
 */
int CheckBox::getOnValue() const
{
    const Q_D(CheckBox);
    return d->onValue;
}

/****************************************************************************/

/** \returns The #offValue.
 */
int CheckBox::getOffValue() const
{
    const Q_D(CheckBox);
    return d->offValue;
}

/****************************************************************************/
