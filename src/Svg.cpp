/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Svg.h"
using Pd::Svg;

#include <QDebug>
#include <QString>
#include <QStyle>
#include <QtGui>
#include <QDomDocument>
#include <QSvgRenderer>


namespace Pd {
class SvgPrivate {
  public:
    SvgPrivate(Pd::Svg *This) : q_ptr(This) {}

  private:

    void loadFile();
    void parseElement(QDomNodeList);
    void printList();

    Svg * const q_ptr;
    QString svgPath; /**< Path to svg image. */
    QDomDocument domDoc;
    struct ElementList {
        QDomElement element;
        QString id;
    };
    QList<ElementList> elementList;

    qreal angle = 0.0; /**< Designer angle. */

    QSvgRenderer imageRenderer;
    bool imageLoaded = false;

    QStringList reqIds;

    Q_DECLARE_PUBLIC(Svg)
};

}

using Pd::SvgPrivate;

/****************************************************************************/
/** Constructor.
 */
Svg::Svg(
        QWidget *parent /**< Parent widget. */
        ):
    QFrame(parent),
    d_ptr(new SvgPrivate(this))
{

}

/****************************************************************************/

/** Destructor.
 */
Svg::~Svg()
{
    // Nothing to do here.
}

/****************************************************************************/

void Svg::setSvgPath(const QString &path)
{
    Q_D(Svg);

    if (d->svgPath == path) {
        return;
    }

    d->svgPath = path;

    if (d->svgPath.isEmpty()) {
        d->imageRenderer.load(QByteArray());
        d->imageLoaded = false;
        d->elementList = QList<SvgPrivate::ElementList>();

    } else {
        d->imageLoaded = d->imageRenderer.load(d->svgPath);
        d->loadFile();
        d->printList();
        update();

    }

}

/****************************************************************************/

/** Method for debug only.
*/
void SvgPrivate::printList()
{
    for (int i = 0; i < elementList.length(); i++) {
        qDebug() << elementList[i].id;
    }
}

/****************************************************************************/

/** Resets the #svgPath.
 */
void Svg::resetSvgPath()
{
    setSvgPath(QString());
}

/****************************************************************************/

void SvgPrivate::loadFile()
{
    QFile file(svgPath);
    domDoc.setContent(&file);
    file.close();

    QDomElement rootElem = domDoc.documentElement();
    parseElement(rootElem.childNodes());

}

/****************************************************************************/

void SvgPrivate::parseElement(QDomNodeList nds)
{
    if (nds.isEmpty()) {
        return;
    }

    for (int i = 0; i < nds.length(); i++) {
        ElementList entry;
        QDomElement elem = nds.at(i).toElement();

        entry.element = elem;
        entry.id = elem.attribute("id");
        elementList.append(entry);

        parseElement(elem.childNodes());
    }
}

/****************************************************************************/

/** Sets the #angle to use.
 */
void Svg::setAngle(qreal a)
{
    Q_D(Svg);

    if (a != d->angle) {
        d->angle = a;
        update();
    }
}

/****************************************************************************/

/** Resets the #angle.
 */
void Svg::resetAngle()
{
    setAngle(0.0);
}

/****************************************************************************/

void Svg::setIdList(const QStringList &list) {
    Q_D(Svg);

    QStringList existIds;
    d->reqIds = list;
}

/****************************************************************************/

QStringList Svg::getIdList() const {
    const Q_D(Svg);

    QStringList ids;
    for (int i = 0; i < d->elementList.length(); i++) {
        ids.append(d->elementList.at(i).id);
    }
    return ids;
}

/****************************************************************************/

bool Svg::existId(QString id) const {
    const Q_D(Svg);

    for (int i = 0; i < d->elementList.length(); i++) {
        if (d->elementList.at(i).id.compare(id) == 0) {
            return true;
        }
    }
    return false;
}

/****************************************************************************/

void Svg::paintEvent(QPaintEvent *event)
{
    Q_D(Svg);
    QFrame::paintEvent(event);

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QSize size = contentsRect().size();
    QRect renderRect(QPoint(), size);

    d->imageRenderer.render(&p, renderRect);
}

/****************************************************************************/

QString Svg::getSvgPath() const
{
    const Q_D(Svg);
    return d->svgPath;
}

/****************************************************************************/

qreal Svg::getAngle() const
{
    const Q_D(Svg);
    return d->angle;
}

/****************************************************************************/
