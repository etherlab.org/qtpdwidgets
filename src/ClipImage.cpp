/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ClipImage.h"

#include <QtGui>
#include <QSvgRenderer>

using namespace Pd;

#define DEFAULT_CLIPMODE Bar

/****************************************************************************/

class Pd::ClipImage::Impl
{
    friend class ClipImage;

    public:
        Impl(ClipImage *parent):
            parent(parent),
            clipMode(DEFAULT_CLIPMODE),
            dataPresent(false),
            value(0.0),
            backgroundRenderer(parent),
            backgroundLoaded(false),
            foregroundRenderer(parent),
            foregroundLoaded(false),
            scaleFactor(0.0)
        {
            updateScale();
        }

        void updateScale()
        {
            /* workaround for designer not accepting QString properties as
             * resources.  try to reload SVG data on next opportunity. */

            if (!backgroundPath.isEmpty() && !backgroundLoaded) {
                backgroundLoaded = backgroundRenderer.load(backgroundPath);
            }
            if (!foregroundPath.isEmpty() && !foregroundLoaded) {
                foregroundLoaded = foregroundRenderer.load(foregroundPath);
            }

            parent->update();
        }

    private:
        ClipImage * const parent;

        QString backgroundPath;
        QString foregroundPath;
        ClipMode clipMode;

        bool dataPresent;
        double value;

        QSvgRenderer backgroundRenderer;
        bool backgroundLoaded;
        QSvgRenderer foregroundRenderer;
        bool foregroundLoaded;

        double scaleFactor;
};

/****************************************************************************/

ClipImage::ClipImage(
        QWidget *parent
        ):
    QFrame(parent),
    impl(std::unique_ptr<ClipImage::Impl>(new Impl(this)))
{
}

/****************************************************************************/

ClipImage::~ClipImage()
{
    clearVariable();
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize ClipImage::sizeHint() const
{
    return QSize(100, 100);
}

/****************************************************************************/

QString ClipImage::getBackground() const
{
    return impl->backgroundPath;
}

/****************************************************************************/

void ClipImage::setBackground(const QString &path)
{
    if (impl->backgroundPath == path) {
        return;
    }

    impl->backgroundPath = path;

    if (path.isEmpty()) {
        impl->backgroundRenderer.load(QByteArray());
        impl->backgroundLoaded = false;
    }
    else {
        impl->backgroundLoaded = impl->backgroundRenderer.load(path);
    }

    impl->updateScale();
}

/****************************************************************************/

void ClipImage::resetBackground()
{
    setBackground(QString());
}

/****************************************************************************/

QString ClipImage::getForeground() const
{
    return impl->foregroundPath;
}

/****************************************************************************/

void ClipImage::setForeground(const QString &path)
{
    if (impl->foregroundPath == path) {
        return;
    }

    impl->foregroundPath = path;

    if (path.isEmpty()) {
        impl->foregroundRenderer.load(QByteArray());
        impl->foregroundLoaded = false;
    }
    else {
        impl->foregroundLoaded = impl->foregroundRenderer.load(path);
    }

    update();
}

/****************************************************************************/

void ClipImage::resetForeground()
{
    setForeground(QString());
}

/****************************************************************************/

ClipImage::ClipMode ClipImage::getClipMode() const
{
    return impl->clipMode;
}

/****************************************************************************/

/** Sets the #clipMode.
 */
void ClipImage::setClipMode(ClipMode m)
{
    impl->clipMode = m;
}

/****************************************************************************/

/** Resets the #clipMode.
 */
void ClipImage::resetClipMode()
{
    setClipMode(DEFAULT_CLIPMODE);
}

/****************************************************************************/

void ClipImage::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    impl->updateScale();
}

/****************************************************************************/

void ClipImage::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);

    QRect renderRect = contentsRect();
    impl->backgroundRenderer.render(&p, renderRect);

    QRectF clipRect(contentsRect());
    double x = 0.0;

    if (impl->dataPresent) {
        x = impl->value;

        if (x < 0.0) {
            x = 0.0;
        }
        else if (x > 1.0) {
            x = 1.0;
        }
    }

    if (impl->clipMode == Bar) {
        //qDebug() << contentsRect() << clipRect << x;
        clipRect.setTop(clipRect.bottom() - x * clipRect.height());
        p.setClipRect(clipRect);
    }
    else if (impl->clipMode == Clock) {
        QPainterPath path;
        path.moveTo(clipRect.center());
        path.lineTo(clipRect.left() + clipRect.width() / 2.0, clipRect.top());
        path.arcTo(clipRect, 90.0, -360.0 * x);
        p.setClipPath(path);
    }

    impl->foregroundRenderer.render(&p, renderRect);
}

/****************************************************************************/

void ClipImage::newValues(std::chrono::nanoseconds)
{
    double v;
    PdCom::details::copyData(&v,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);

    if (impl->dataPresent) {
        double newValue;

        if (getFilterConstant() > 0.0) {
            newValue = getFilterConstant() * (v - impl->value) + impl->value;
        } else {
            newValue = v;
        }

        impl->value = newValue;
    } else {
        impl->value = v; // bypass filter
        impl->dataPresent = true;
    }

    impl->parent->update();
}

/****************************************************************************/

void ClipImage::stateChange(PdCom::Subscription::State state)
{
    if (state != PdCom::Subscription::State::Active) {
        impl->dataPresent = false;
        impl->parent->update();
    }
}

/****************************************************************************/
