/*****************************************************************************
 *
 * Copyright (C) 2009 - 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Led.h"
using Pd::Led;

#include <QtGui>

/****************************************************************************/

#define DEFAULT_ONCOLOR      Qt::green
#define DEFAULT_OFFCOLOR     Qt::red
#define DEFAULT_OFFCOLORMODE DarkOnColor
#define DEFAULT_INVERT       false

/****************************************************************************/

struct Led::Impl
{
    Impl(Led *parent):
        parent{parent},
        onColor{DEFAULT_ONCOLOR},
        offColor{DEFAULT_OFFCOLOR},
        offColorMode{DEFAULT_OFFCOLORMODE},
        invert{DEFAULT_INVERT}
    {
        updateCurrentOffColor();
        retranslate();
    }

    Led * const parent;

    QColor onColor; /**< The color to display, when the LED is on. */
    QColor offColor; /**< The color to display, when the LED is off.
                       This is only used, if #offColorMode
                       is #ExplicitOffColor. */
    OffColorMode offColorMode; /**< Determines, which color to use,
                                 when the LED is off. */
    QColor currentOffColor; /**< Current Off-Color, depending
                              on #onColor, #offColor and #offColorMode. */
    bool invert; /**< Inversion flag. This inverts the value returned
                   by ledOn(). */

    /** Calculates the #currentOffColor.
     */
    void updateCurrentOffColor()
    {
        QColor newOffColor;

        switch (offColorMode) {
            case DarkOnColor:
                newOffColor = onColor.darker(300);
                break;
            case ExplicitOffColor:
                newOffColor = offColor;
                break;
        }

        if (newOffColor == currentOffColor) {
            return;
        }

        currentOffColor = newOffColor;
        parent->updateColor();
    }

    /** Retranslate the widget.
     */
    void retranslate()
    {
        parent->setWindowTitle(Pd::Led::tr("LED"));
    }

};

/****************************************************************************/

/** Constructor.
 */
Led::Led(
        QWidget *parent /**< Parent widget. */
        ): MultiLed(parent),
    impl{std::unique_ptr<Impl>{new Impl(this)}}
{
}

/****************************************************************************/

/** Destructor.
 */
Led::~Led()
{
}

/****************************************************************************/

/**
 * \returns The #onColor.
 */
QColor Led::getOnColor() const
{
    return impl->onColor;
}

/****************************************************************************/

/** Sets the #onColor.
 */
void Led::setOnColor(QColor c)
{
    if (c != impl->onColor) {
        impl->onColor = c;
        impl->updateCurrentOffColor();
        updateColor();
    }
}

/****************************************************************************/

/** Resets the #onColor.
 */
void Led::resetOnColor()
{
    setOnColor(DEFAULT_ONCOLOR);
}

/****************************************************************************/

/**
 * \returns The #offColor.
 */
QColor Led::getOffColor() const
{
    return impl->offColor;
}

/****************************************************************************/

/** Sets the #offColor.
 */
void Led::setOffColor(QColor c)
{
    if (c != impl->offColor) {
        impl->offColor = c;
        impl->updateCurrentOffColor();
    }
}

/****************************************************************************/

/** Resets the #offColor.
 */
void Led::resetOffColor()
{
    setOffColor(DEFAULT_OFFCOLOR);
}

/****************************************************************************/

/**
 * \returns The #offColorMode.
 */
Led::OffColorMode Led::getOffColorMode() const
{
    return impl->offColorMode;
}

/****************************************************************************/

/** Sets the #offColorMode.
 */
void Led::setOffColorMode(OffColorMode m)
{
    if (m != impl->offColorMode) {
        impl->offColorMode = m;
        impl->updateCurrentOffColor();
    }
}

/****************************************************************************/

/** Resets the #offColorMode.
 */
void Led::resetOffColorMode()
{
    setOffColorMode(DEFAULT_OFFCOLORMODE);
}

/****************************************************************************/

/**
 * \returns The #invert flag.
 */
bool Led::getInvert() const
{
    return impl->invert;
}

/****************************************************************************/

/** Sets the #invert flag.
 */
void Led::setInvert(bool i)
{
    if (i != impl->invert) {
        impl->invert = i;
        updateColor();
    }
}

/****************************************************************************/

/** Resets the #invert flag.
 */
void Led::resetInvert()
{
    setInvert(DEFAULT_INVERT);
}

/****************************************************************************/

/** Event handler.
 */
bool Led::event(
        QEvent *event /**< Paint event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        impl->retranslate();
    }

    return MultiLed::event(event);
}

/****************************************************************************/

/** Calculates the LED color.
 */
void Led::updateColor()
{
    Value newValue;
    newValue.blink = Value::Steady;

    if (hasData()) {
        newValue.color = (getValue() ^ impl->invert) ?
            impl->onColor : impl->currentOffColor;
    } else {
        newValue.color = disconnectColor;
    }

    setCurrentValue(newValue);
}

/****************************************************************************/
