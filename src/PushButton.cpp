/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include <QtGui>

#include "PushButton.h"

using Pd::PushButton;

/****************************************************************************/

#define DEFAULT_BUTTONMODE     PressRelease
#define DEFAULT_EVENTCONDITION OnClicked
#define DEFAULT_EVENTACTION    IncrementValue
#define DEFAULT_ONVALUE        1
#define DEFAULT_OFFVALUE       0

/****************************************************************************/

struct PushButton::Impl
{
    Impl(PushButton *parent):
        parent{parent},
        value{0},
        pressed{false},
        buttonMode{DEFAULT_BUTTONMODE},
        eventCondition{DEFAULT_EVENTCONDITION},
        eventAction{DEFAULT_EVENTACTION},
        onValue{DEFAULT_ONVALUE},
        offValue{DEFAULT_OFFVALUE}
    {
    }

    PushButton * const parent;

    int32_t value; /**< The current value of the process variable. */
    bool pressed; /**< \a true, when the button is pressed down. */
    ButtonMode buttonMode; /**< The button behaviour. */
    EventCondition eventCondition; /**< Event condition. */
    EventAction eventAction; /**< Action on an event. */
    int onValue; /**< The value to be written, when the button is
                   pressed. */
    int offValue; /**< The value to be written, when the button is
                    released. */

    /** This function executes the action specified in #eventAction.
     */
    void triggerEvent()
    {
        switch (eventAction) {
            case IncrementValue:
                {
                    int newValue = value + 1;
                    parent->writeValue(newValue);
                }
                break;

            case SetOnValue:
                parent->writeValue(onValue);
                break;

            case ToggleValue:
                if (value == onValue) {
                    parent->writeValue(offValue);
                }
                else {
                    parent->writeValue(onValue);
                }
                break;
        }
    }

};

/****************************************************************************/

/** Constructor.
 */
PushButton::PushButton(
        QWidget *parent /**< parent widget */
        ): QPushButton(parent),
    impl{std::unique_ptr<Impl>(new Impl(this))}
{
    connect(this, SIGNAL(clicked()), this, SLOT(on_clicked()));
    connect(this, SIGNAL(pressed()), this, SLOT(on_pressed()));
    connect(this, SIGNAL(released()), this, SLOT(on_released()));
}

/****************************************************************************/

/** Destructor.
 */
PushButton::~PushButton()
{
}

/****************************************************************************/

void PushButton::clearData()
{
}

/****************************************************************************/

/**
 * \return The #buttonMode.
 */
PushButton::ButtonMode PushButton::getButtonMode() const
{
    return impl->buttonMode;
}

/****************************************************************************/

/** Sets the #buttonMode.
 *
 * If the button is currently pressed down and the button is
 * in #PressRelease mode (very rare), an released() signal is generated.
 */
void PushButton::setButtonMode(ButtonMode m)
{
    if (m != impl->buttonMode) {
        if (impl->buttonMode == PressRelease) {
            on_released();
        }

        impl->buttonMode = m;
    }
}

/****************************************************************************/

/** Resets the #buttonMode.
 */
void PushButton::resetButtonMode()
{
    setButtonMode(DEFAULT_BUTTONMODE);
}

/****************************************************************************/

/**
 * \return The #eventCondition.
 */
PushButton::EventCondition PushButton::getEventCondition() const
{
    return impl->eventCondition;
}

/****************************************************************************/

/** Sets the #eventCondition.
 */
void PushButton::setEventCondition(EventCondition c)
{
    impl->eventCondition = c;
}

/****************************************************************************/

/** Resets the #eventCondition.
 */
void PushButton::resetEventCondition()
{
    setEventCondition(DEFAULT_EVENTCONDITION);
}

/****************************************************************************/

/**
 * \return The #eventAction.
 */
PushButton::EventAction PushButton::getEventAction() const
{
    return impl->eventAction;
}

/****************************************************************************/

/** Sets the #eventAction.
 */
void PushButton::setEventAction(EventAction a)
{
    impl->eventAction = a;
}

/****************************************************************************/

/** Resets the #eventAction.
 */
void PushButton::resetEventAction()
{
    setEventAction(DEFAULT_EVENTACTION);
}

/****************************************************************************/

/**
 * \return The #onValue.
 */
int PushButton::getOnValue() const
{
    return impl->onValue;
}

/****************************************************************************/

/** Sets the #onValue.
 */
void PushButton::setOnValue(int v)
{
    impl->onValue = v;
}

/****************************************************************************/

/** Resets the #onValue.
 */
void PushButton::resetOnValue()
{
    setOnValue(DEFAULT_ONVALUE);
}

/****************************************************************************/

/**
 * \return The #offValue.
 */
int PushButton::getOffValue() const
{
    return impl->offValue;
}

/****************************************************************************/

/** Sets the #offValue.
 */
void PushButton::setOffValue(int v)
{
    impl->offValue = v;
}

/****************************************************************************/

/** Resets the #offValue.
 */
void PushButton::resetOffValue()
{
    setOffValue(DEFAULT_OFFVALUE);
}

/****************************************************************************/

/** The button has been clicked.
 *
 * This is the case, when the user presses the button and releases it with the
 * curser inside the button area.
 */
void PushButton::on_clicked()
{
    if (impl->buttonMode == Event && impl->eventCondition == OnClicked) {
        impl->triggerEvent();
    }
    else if (impl->buttonMode == PressRelease && isCheckable()) {
        if (isChecked()) {
            writeValue(impl->onValue);
        }
        else {
            writeValue(impl->offValue);
        }
    }
}

/****************************************************************************/

/** The button is pressed down.
 */
void PushButton::on_pressed()
{
    impl->pressed = true;

    if (impl->buttonMode == PressRelease && !isCheckable()) {
        writeValue(impl->onValue);
    }
    else if (impl->buttonMode == Event && impl->eventCondition == OnPressed) {
        impl->triggerEvent();
    }
}

/****************************************************************************/

/** The button is released.
 */
void PushButton::on_released()
{
    if (impl->pressed) {
        impl->pressed = false;

        if (impl->buttonMode == PressRelease && !isCheckable()) {
            writeValue(impl->offValue);
        }
        else if (impl->buttonMode == Event &&
                impl->eventCondition == OnReleased) {
            impl->triggerEvent();
        }
    }
}

/****************************************************************************/

/** This function executes the action specified in #eventAction.
 */
void PushButton::changeEvent(QEvent *event)
{
    if (impl->pressed && !isEnabled()) {
        // if button is disabled while pressed, release it!
        on_released();
    }

    QPushButton::changeEvent(event);
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void PushButton::newValues(std::chrono::nanoseconds)
{
    int32_t newValue;
    PdCom::details::copyData(&newValue,
            PdCom::details::TypeInfoTraits<int32_t>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    impl->value = newValue * scale + offset;

    if (impl->buttonMode == PressRelease && isCheckable()) {
        setChecked(impl->value == impl->onValue);
    }
}

/****************************************************************************/
