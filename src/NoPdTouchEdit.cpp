/*****************************************************************************
 *
 * Copyright (C)        2013  Florian Pose <fp@igh.de>
 *               2011 - 2012  Andreas Stewering-Bone <ab@igh-essen.com>
 *                            Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "NoPdTouchEdit.h"

/****************************************************************************/

#define DEFAULT_VALUE       0
#define DEFAULT_ALIGNMENT   (Qt::AlignRight | Qt::AlignVCenter)
#define DEFAULT_DECIMALS    0
#define DEFAULT_SUFFIX      ""
#define DEFAULT_LOWERLIMIT  (-std::numeric_limits<double>::infinity())
#define DEFAULT_UPPERLIMIT  (std::numeric_limits<double>::infinity())

/****************************************************************************/

namespace Pd {
class NoPdTouchEditPrivate {
    NoPdTouchEditPrivate(NoPdTouchEdit *parent) : q_ptr(parent) {}

    NoPdTouchEdit *const q_ptr;
    double value = DEFAULT_VALUE; /**< Current value. */
    Qt::Alignment alignment = DEFAULT_ALIGNMENT; /**< Text alignment. */
    quint32 decimals = DEFAULT_DECIMALS; /**< Number of decimal digits. */
    QString suffix = DEFAULT_SUFFIX; /**< Suffix, that is appended to the displayed
                        string. The suffix is appended without a separator
                        (like in other Qt classes), so if you want to
                        specify a unit, you'll have to set suffix to
                        " kN", for example. */
    double lowerLimit = DEFAULT_LOWERLIMIT;
    double upperLimit = DEFAULT_UPPERLIMIT;
    int editDigit     = 0;

    TouchEditDialog *editDialog = nullptr;

    void openDialog();
    void retranslate();

    Q_DECLARE_PUBLIC(NoPdTouchEdit);
};
}

using Pd::NoPdTouchEdit;
using Pd::NoPdTouchEditPrivate;

#include "TouchEditDialog.h"

#include <QtGui>
#include <QDebug>

#include <limits>


/** Constructor.
 */
NoPdTouchEdit::NoPdTouchEdit(
        QWidget *parent /**< Parent widget. */
        ): QFrame(parent),
    d_ptr(new NoPdTouchEditPrivate(this))
{
    Q_D(NoPdTouchEdit);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    d->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
NoPdTouchEdit::~NoPdTouchEdit()
{
}

/****************************************************************************/

/** Sets the text #value.
 */
void NoPdTouchEdit::setValue(double v)
{
    Q_D(NoPdTouchEdit);
    if (d->value != v) {
        d->value = v;
        emit valueChanged();
        update(contentsRect());
    }
}

/****************************************************************************/

/** Resets the #value.
 */
void NoPdTouchEdit::resetValue()
{
    setValue(DEFAULT_VALUE);
}

/****************************************************************************/

/** Sets the text #alignment.
 */
void NoPdTouchEdit::setAlignment(Qt::Alignment a)
{
    Q_D(NoPdTouchEdit);
    if (d->alignment != a) {
        d->alignment = a;
        update(contentsRect());
    }
}

/****************************************************************************/

/** Resets the number of #decimals.
 */
void NoPdTouchEdit::resetAlignment()
{
    setAlignment(DEFAULT_ALIGNMENT);
}

/****************************************************************************/

/** Sets the number of #decimals.
 */
void NoPdTouchEdit::setDecimals(quint32 decimals)
{
    Q_D(NoPdTouchEdit);
    if (decimals != d->decimals) {
        d->decimals = decimals;
        update(contentsRect());
    }
}

/****************************************************************************/

/** Resets the number of #decimals.
 */
void NoPdTouchEdit::resetDecimals()
{
    setDecimals(DEFAULT_DECIMALS);
}

/****************************************************************************/

/** Sets the #suffix to display after the value.
 */
void NoPdTouchEdit::setSuffix(const QString &suffix)
{
    Q_D(NoPdTouchEdit);
    if (suffix != d->suffix) {
        d->suffix = suffix;
        update(contentsRect());
    }
}

/****************************************************************************/

/** Resets the #suffix to display after the value.
 */
void NoPdTouchEdit::resetSuffix()
{
    setSuffix(DEFAULT_SUFFIX);
}

/****************************************************************************/

/** Sets the lowerLimit.
 */
void NoPdTouchEdit::setLowerLimit(double limit)
{
    Q_D(NoPdTouchEdit);
    if (limit != d->lowerLimit) {
        d->lowerLimit = limit;
    }
}

/****************************************************************************/

/** Resets the lowerLimit.
 */
void NoPdTouchEdit::resetLowerLimit()
{
    setLowerLimit(DEFAULT_LOWERLIMIT);
}

/****************************************************************************/

/** Sets the upperLimit.
 */
void NoPdTouchEdit::setUpperLimit(double limit)
{
    Q_D(NoPdTouchEdit);
    if (limit != d->upperLimit) {
        d->upperLimit = limit;
    }
}

/****************************************************************************/

/** Resets the upperLimit.
 */
void NoPdTouchEdit::resetUpperLimit()
{
    setUpperLimit(DEFAULT_UPPERLIMIT);
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize NoPdTouchEdit::sizeHint() const
{
    return QSize(60, 25);
}

/****************************************************************************/

/** Eventhandler.
 */
bool NoPdTouchEdit::event(QEvent *event)
{
    Q_D(NoPdTouchEdit);
    switch (event->type()) {
        case QEvent::MouseButtonPress:
            d->openDialog();
            return true;

        case QEvent::LanguageChange:
            d->retranslate();
            break;

        default:
            break;
    }

    return QFrame::event(event);
}

/****************************************************************************/

/** Paint function.
 */
void NoPdTouchEdit::paintEvent(
        QPaintEvent *event /**< Paint event flags. */
        )
{
    QFrame::paintEvent(event);
    QPainter painter(this);
    drawText(event, painter);
}

/****************************************************************************/

/** Draws the text.
 */
void NoPdTouchEdit::drawText(
        QPaintEvent *event, /**< Paint event flags. */
        QPainter &painter /**< Painter. */
        )
{
    Q_D(NoPdTouchEdit);
    if (event->rect().intersects(contentsRect())) {
        QString displayText = QLocale().toString(d->value, 'f', d->decimals);
        displayText += d->suffix;
        painter.drawText(contentsRect(), d->alignment, displayText);
    }
}

/****************************************************************************/

/** Opens the edit dialog.
 */
void NoPdTouchEditPrivate::openDialog()
{
    Q_Q(NoPdTouchEdit);
    if (!q->isEnabled()) {
        return;
    }

    if (!editDialog) { // create the dialog
        editDialog = new Pd::TouchEditDialog(q);
    }

    editDialog->setValue(value);
    editDialog->setLowerLimit(lowerLimit);
    editDialog->setUpperLimit(upperLimit);
    editDialog->setSuffix(suffix);
    editDialog->setDecimals(decimals);
    editDialog->setEditDigit(editDigit);

    if (editDialog->exec()) {
        q->setValue(editDialog->getValue());
        editDigit = editDialog->getEditDigit(); // remember last edited digit
    }
}

/****************************************************************************/

/** React to state changes.
 */
void NoPdTouchEdit::changeEvent(QEvent *event)
{
    Q_D(NoPdTouchEdit);
    if (event->type() == QEvent::EnabledChange) {
        if(!isEnabled() && d->editDialog && d->editDialog->isVisible()) {
            /* close the editDialog if it is open
             * while we receive a disable. */
            d->editDialog->done(QDialog::Rejected);
        }

        update();
    }

    QFrame::changeEvent(event);
}

/****************************************************************************/

/** Retranslate the widget.
 */
void NoPdTouchEditPrivate::retranslate()
{
    Q_Q(NoPdTouchEdit);
    q->setWindowTitle(Pd::NoPdTouchEdit::tr("Touch entry"));
}

/****************************************************************************/

double NoPdTouchEdit::getValue() const
{
    const Q_D(NoPdTouchEdit);
    return d->value;
}

Qt::Alignment NoPdTouchEdit::getAlignment() const
{
    const Q_D(NoPdTouchEdit);
    return d->alignment;
}

QString NoPdTouchEdit::getSuffix() const
{
    const Q_D(NoPdTouchEdit);
    return d->suffix;
}

double NoPdTouchEdit::getLowerLimit() const
{
    const Q_D(NoPdTouchEdit);
    return d->lowerLimit;
}

double NoPdTouchEdit::getUpperLimit() const
{
    const Q_D(NoPdTouchEdit);
    return d->upperLimit;
}

quint32 NoPdTouchEdit::getDecimals() const
{
    const Q_D(NoPdTouchEdit);
    return d->decimals;
}
