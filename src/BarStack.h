/*****************************************************************************
 *
 * Copyright (C) 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_BAR_STACK_H
#define PD_BAR_STACK_H

#include <QtGui>
#include <QList>

#include "Bar_p.h"

/****************************************************************************/
namespace Pd {

class BarPrivate::Stack
{
    public:
        Stack(BarPrivate *);
        virtual ~Stack();

        void addSection(PdCom::Variable, const PdCom::Selector &,
                const QtPdCom::Transmission & = QtPdCom::event_mode, double = 1.0,
                double = 0.0, double = 0.0, QColor = Qt::black);
        void addSection(PdCom::Process *, const QString &,
                const PdCom::Selector &,
                const QtPdCom::Transmission & = QtPdCom::event_mode, double = 1.0,
                double = 0.0, double = 0.0, QColor = Qt::black);
        void clearData();

        QRect &getRect() { return rect; }
        const QRect &getRect() const { return rect; }

        void paint(QPainter &);

        void redrawEvent();
        void update();

    private:
        BarPrivate * const bar;
        QRect rect;

        class Section;
        typedef QList<Section *> SectionList;
        SectionList sections;

        void paintColorBar(QPainter &);
        void paintArrow(QPainter &);
        const QColor findMultiColor(double) const;

        Stack();
};

}

#endif

/****************************************************************************/
