/*****************************************************************************
 *
 * Copyright (C) 2009  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "RadioButton.h"
using Pd::RadioButton;

#include <QtGui>

/****************************************************************************/

#define DEFAULT_CHECKVALUE 1

/****************************************************************************/

struct RadioButton::Impl
{
    Impl(RadioButton *parent):
        parent{parent},
        dataPresent{nullptr},
        checkValue{DEFAULT_CHECKVALUE}
    {
    }

    RadioButton * const parent;

    bool dataPresent; /**< A value shall be displayed. */
    int value; /**< the current variable value */
    int checkValue; /**< The value that is sent to the process, if the
                      button is checked. */

    void updateCheck()
    {
        if (dataPresent && value == checkValue) {
            parent->setChecked(true);
        } else {
            parent->setChecked(false);
        }
    }

};

/****************************************************************************/

/** Constructor.
 */
RadioButton::RadioButton(
        QWidget *parent /**< parent widget */
        ): QRadioButton(parent),
    impl{std::unique_ptr<Impl>{new Impl(this)}}
{
    setAutoExclusive(false); // needed to uncheck
}

/****************************************************************************/

/** Destructor.
 */
RadioButton::~RadioButton()
{
}

/****************************************************************************/

void RadioButton::clearData()
{
    impl->dataPresent = false;
    impl->updateCheck();
}

/****************************************************************************/

/** \returns The #checkValue.
 */
int RadioButton::getCheckValue() const
{
    return impl->checkValue;
}

/****************************************************************************/

/** Sets the #checkValue.
 */
void RadioButton::setCheckValue(int v)
{
    if (v != impl->checkValue) {
        impl->checkValue = v;
        impl->updateCheck();
    }
}

/****************************************************************************/

/** Resets the #checkValue.
 */
void RadioButton::resetCheckValue()
{
    setCheckValue(DEFAULT_CHECKVALUE);
}

/****************************************************************************/

/** Sets the next check state.
 *
 * This virtual method originating from QAbstractButton is called, when the
 * user requests a new check state (by clicking on the button, or by hitting
 * the acceleration key, etc.). It is designed to let subclasses implement
 * intermediate check states.
 *
 * Here it is used to write the new state to the process first and to delay
 * the setting of the new check state until the process variable was written.
 */
void RadioButton::nextCheckState()
{
    if (!isChecked()) {
        writeValue(impl->checkValue);
    }
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void RadioButton::newValues(std::chrono::nanoseconds)
{
    int32_t newValue;
    PdCom::details::copyData(&newValue,
            PdCom::details::TypeInfoTraits<int32_t>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    newValue = newValue * scale + offset;

    if (newValue != impl->value || !impl->dataPresent) {
        impl->value = newValue;
        impl->dataPresent = true;
        impl->updateCheck();
    }
}

/****************************************************************************/
