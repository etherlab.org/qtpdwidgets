/*****************************************************************************
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *               2023 Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_PARAMETER_H
#define PD_PARAMETER_H

#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>

#include <QtPdCom1/Process.h>

#include <QUrl>
#include <QJsonValue>
#include <QVariantList>

namespace Pd
{
    class ParameterSetWidget;

    class Parameter:
        public QObject, public PdCom::Subscriber
    {
        Q_OBJECT

      public:
        Parameter(Pd::ParameterSetWidget *);
        virtual ~Parameter();

        QUrl getUrl() const { return url; }
        void setUrl(QUrl newUrl) { url = newUrl; }

        void fromJson(const QJsonValue &);

        void replaceUrl(const QUrl &, const QUrl &);

        PdCom::Subscription *getSubscription() { return &subscription; }
        PdCom::Variable *getVariable() { return &pv; }

        bool hasData() const { return dataPresent; }
        QVector<double> getCurrentValue() const { return currentItems; }
        bool setCurrentValue(const QVector<double> &);

        bool hasSavedData() const { return savedDataPresent; }
        QVector<double> getSavedValue() const { return savedItems; }
        bool setSavedValue(const QVector<double> &);

        void connectParameter(QtPdCom::Process *);

      signals:
        void dataChanged();

      private:
        Pd::ParameterSetWidget * const widget;

        QUrl url;
        QString connectionPath;
        PdCom::Subscription subscription;
        PdCom::Variable pv;
        QVector<double> savedItems;
        QVector<double> currentItems;
        double scale;
        double offset;
        bool dataPresent;
        bool savedDataPresent;

        // virtual from PdCom::Subscriber
        void newValues(std::chrono::nanoseconds) override;
        void stateChanged(const PdCom::Subscription &) override;

        Parameter();
    };

    /************************************************************************/

}  // namespace Pd

#endif
