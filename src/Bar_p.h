/*****************************************************************************
 *
 * Copyright (C) 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_BAR_PRIVATE_H
#define PD_BAR_PRIVATE_H

#include <QWidget>
#include <QPixmap>

#include "Bar.h"
#include "Export.h"

#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/Transmission.h>
#include "Widget.h"
#include "Scale.h"

namespace Pd {

/****************************************************************************/

/** Bar graph widget.
 */
class BarPrivate : public Widget
{
    Q_DECLARE_PUBLIC(Bar)
    Bar *const q_ptr;


        BarPrivate(Bar *parent);

        /** Clear all stacks and sections.
         */
        void clearVariables();

    private:
        Bar::Orientation orientation; /**< Orientation of the bar */
        Bar::Style style; /**< Widget appearance. */
        bool showScale; /**< Whether or not the scale is shown. */
        Scale valueScale; /**< Value scale. */
        Bar::Origin origin; /**< Bar drawing origin. */
        int borderWidth; /**< Width of the border around the bar drawing area.
                          */
        QColor backgroundColor; /**< Background color of the bar area. */
        bool autoBarWidth;
        QGradientStops gradientStops; /**< Color gradient. */
        QLinearGradient gradient;

        double minStop; /**< Value of first gradient stop */
        double maxStop; /**< Value of last gradient stop */

        class Stack;
        typedef QList<Stack *> StackList;
        StackList stacks; /**< Bar stacks. */

        QPixmap backgroundPixmap; /**< Pixmap that stores the background and
                                    scale. */
        QRect scaleRect;
        QRect borderRect;
        QRect barRect; /**< Layout rectangle containing the bar drawing
                         area. */
        QLine zeroLine; /**< Zero line. */
        QPolygon darkPolygon; /**< Polygon to draw the border shadow. */
        QPolygon brightPolygon; /**< Polygon to draw the border light. */

        int maxBarWidth;

        QString debugStr;

        static QPolygon verticalDragIndicatorPolygon;
        static QPolygon horizontalDragIndicatorPolygon;
        static QPolygon verticalArrow;
        static QPolygon horizontalArrow;
        static void initDragIndicatorPolygons();

        void updateLayout();
        void updateBackground();
        unsigned int calcPosition(double, bool = true) const;

        void notifyMaxBarWidthChange();
        QList<Bar *> findSiblings();
        void maxBarWidthChanged();
        void retranslate();

        void updateGradient();
        void redrawEvent();
};

/****************************************************************************/

} // namespace Pd

#endif
