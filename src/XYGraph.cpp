/*****************************************************************************
 *
 * Copyright (C) 2009 - 2019  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "XYGraph.h"
using Pd::XYGraph;

#include "Scale.h"
#include <QtPdCom1/ScalarSubscriber.h>

#include <QtGui>

/****************************************************************************/

#define DEFAULT_TIMERANGE 1.0
#define DEFAULT_SCALEXMIN -10.0
#define DEFAULT_SCALEXMAX 10.0
#define DEFAULT_SCALEYMIN -10.0
#define DEFAULT_SCALEYMAX 10.0
#define DEFAULT_LINE_WIDTH 1
#define DEFAULT_LINE_COLOR QColor( 38, 139, 210) // solarized blue

/****************************************************************************/

/** Axis specification.
 */
enum AxisOrientation {
    X, /**< The Horizontal axis. */
    Y, /**< The vertical axis. */
    NumAxes /**< Number of axes. */
};

/****************************************************************************/

struct XYGraph::Impl
{
    Impl(XYGraph *);
    ~Impl();

    XYGraph * const graph;

    /** Time/value pair type.
     */
    struct TimeValuePair {
        std::chrono::nanoseconds time; /**< Time. */
        double value; /**< Value. */
    };

    /** Axis class.
     */
    class Axis:
        public QtPdCom::ScalarSubscriber
    {
        public:
            XYGraph::Impl * const impl;
            QList<TimeValuePair> values; /**< List of values. */

            Axis(XYGraph::Impl *impl);

            void newValues(std::chrono::nanoseconds) override;
            void stateChange(PdCom::Subscription::State) override;

            void removeDeprecated();
    };

    std::vector<Axis *> axes; /**< Axes. */

    double timeRange; /**< See the #timeRange property. */
    Scale xScale; /**< X Scale. */
    Scale yScale; /**< Y Scale. */
    QRect contRect; /**< Contents area. This is the rectangle inside the
                      frame borders. */
    QRect rect[NumAxes]; /**< Areas for scales and data. */
    QRect graphRect; /**< Graph area. */

    /** XY point type. */
    struct Point {
        double value[NumAxes]; /**< Value for each axis. */
        std::chrono::nanoseconds time; /**< Time of the point. */
    };
    QList<Point> points; /**< Extracted points. */

    int lineWidth;
    QColor lineColor;

    void updateRects();
    void extractPoints();
    void removeDeprecated();

    void retranslate();
};

/****************************************************************************/

/** Constructor.
 */
XYGraph::XYGraph(
        QWidget *parent /**< parent widget */
        ):
    QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setMinimumSize(50, 50);

    impl->retranslate();
}

/****************************************************************************/

/** Destructor.
 */
XYGraph::~XYGraph()
{
    clearVariables();
}

/****************************************************************************/

/** Subscribes the horizontal/vertical channel to a ProcessVariable.
 *
 * The first call connects the horizontal channel to the given
 * ProcessVariable. The second call will connect the vertical channel to the
 * given ProcessVariable. Any further call will be ignored.
 *
 * \todo Add channel paremeter.
 */
void XYGraph::addVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const QtPdCom::Transmission &transmission,
        double gain,
        double offset
        )
{
    if (pv.empty()) {
        return;
    }

    for (int index = X; index < NumAxes; index++) {
        auto axis = impl->axes[index];
        if (!axis->hasVariable()) {
            axis->setVariable(pv, selector, transmission, gain, offset);
            return;
        }
    }
}

/****************************************************************************/

/** Subscribes the horizontal/vertical channel to a ProcessVariable.
 *
 * The first call connects the horizontal channel to the given
 * ProcessVariable. The second call will connect the vertical channel to the
 * given ProcessVariable. Any further call will be ignored.
 *
 * \todo Add channel paremeter.
 */
void XYGraph::addVariable(
        PdCom::Process *p,
        const QString &path,
        const PdCom::Selector &selector,
        const QtPdCom::Transmission &transmission,
        double gain,
        double offset
        )
{
    if (not p or path.isEmpty()) {
        return;
    }

    for (int index = X; index < NumAxes; index++) {
        auto axis = impl->axes[index];
        if (!axis->hasVariable()) {
            axis->setVariable(p, path, selector, transmission, gain, offset);
            return;
        }
    }
}

/****************************************************************************/

/** Unsubscribes from any subscribed ProcessVariables.
 */
void XYGraph::clearVariables()
{
    for (int index = X; index < NumAxes; index++) {
        impl->axes[index]->clearVariable();
    }
}

/****************************************************************************/

/** Clears any displayed data.
 */
void XYGraph::clearData()
{
    if (impl->points.count()) {
        impl->points.clear();
        update();
    }
}

/****************************************************************************/

/**
 * \return The #timeRange.
 */
double XYGraph::getTimeRange() const
{
    return impl->timeRange;
}

/****************************************************************************/

/** Sets the #timeRange.
 */
void XYGraph::setTimeRange(double range)
{
    if (range != impl->timeRange) {
        impl->timeRange = range;
        impl->removeDeprecated();
        update();
    }
}

/****************************************************************************/

/** Resets the #timeRange.
 */
void XYGraph::resetTimeRange()
{
    setTimeRange(DEFAULT_TIMERANGE);
}

/****************************************************************************/

/**
 * \return The minimum value of the horizontal scale.
 */
double XYGraph::getScaleXMin() const
{
    return impl->xScale.getMin();
}

/****************************************************************************/

/** Sets the minimum value of the horizontal scale.
 */
void XYGraph::setScaleXMin(double scale)
{
    if (scale != impl->xScale.getMin()) {
        impl->xScale.setMin(scale);
        impl->updateRects();
    }
}

/****************************************************************************/

/** Resets the minimum value of the horizontal scale.
 */
void XYGraph::resetScaleXMin()
{
    setScaleXMin(DEFAULT_SCALEXMIN);
}

/****************************************************************************/

/**
 * \return The maximum value of the horizontal scale.
 */
double XYGraph::getScaleXMax() const
{
    return impl->xScale.getMax();
}

/****************************************************************************/

/** Sets the maximum value of the horizontal scale.
 */
void XYGraph::setScaleXMax(double scale)
{
    if (scale != impl->xScale.getMax()) {
        impl->xScale.setMax(scale);
        impl->updateRects();
    }
}

/****************************************************************************/

/** Resets the maximum value of the horizontal scale.
 */
void XYGraph::resetScaleXMax()
{
    setScaleXMax(DEFAULT_SCALEXMAX);
}

/****************************************************************************/

/**
 * \return The minimum value of the vertical scale.
 */
double XYGraph::getScaleYMin() const
{
    return impl->yScale.getMin();
}

/****************************************************************************/

/** Sets the minimum value of the vertical scale.
 */
void XYGraph::setScaleYMin(double scale)
{
    if (scale != impl->yScale.getMin()) {
        impl->yScale.setMin(scale);
        impl->updateRects();
    }
}

/****************************************************************************/

/** Resets the minimum value of the vertical scale.
 */
void XYGraph::resetScaleYMin()
{
    setScaleYMin(DEFAULT_SCALEYMIN);
}

/****************************************************************************/

/**
 * \return The maximum value of the vertical scale.
 */
double XYGraph::getScaleYMax() const
{
    return impl->yScale.getMax();
}

/****************************************************************************/

/** Sets the maximum value of the vertical scale.
 */
void XYGraph::setScaleYMax(double scale)
{
    if (scale != impl->yScale.getMax()) {
        impl->yScale.setMax(scale);
        impl->updateRects();
    }
}

/****************************************************************************/

/** Resets the maximum value of the vertical scale.
 */
void XYGraph::resetScaleYMax()
{
    setScaleYMax(DEFAULT_SCALEYMAX);
}

/****************************************************************************/

/**
 * \return The line width.
 */
int XYGraph::getLineWidth() const
{
    return impl->lineWidth;
}

/****************************************************************************/

/** Sets the line width.
 */
void XYGraph::setLineWidth(int width)
{
    if (width != impl->lineWidth) {
        impl->lineWidth = width;
        update();
    }
}

/****************************************************************************/

/** Resets the line width.
 */
void XYGraph::resetLineWidth()
{
    setLineWidth(DEFAULT_LINE_WIDTH);
}

/****************************************************************************/

/**
 * \return The line color.
 */
QColor XYGraph::getLineColor() const
{
    return impl->lineColor;
}

/****************************************************************************/

/** Sets line color.
 */
void XYGraph::setLineColor(const QColor &color)
{
    if (color != impl->lineColor) {
        impl->lineColor = color;
        update();
    }
}

/****************************************************************************/

/** Resets the line color.
 */
void XYGraph::resetLineColor()
{
    setLineColor(DEFAULT_LINE_COLOR);
}

/****************************************************************************/

/** Gives a hint aboute the optimal size.
 */
QSize XYGraph::sizeHint() const
{
    return QSize(150, 150);
}

/****************************************************************************/

/** Event handler.
 */
bool XYGraph::event(
        QEvent *event /**< Paint event flags. */
        )
{
    if (event->type() == QEvent::LanguageChange) {
        impl->retranslate();
    }

    return QFrame::event(event);
}

/****************************************************************************/

/** Handles the widget's resize event.
 */
void XYGraph::resizeEvent(
        QResizeEvent *event /**< Resize event. */
        )
{
    impl->contRect = contentsRect();
    impl->updateRects();
    QFrame::resizeEvent(event);
}

/****************************************************************************/

/** Paint function.
 */
void XYGraph::paintEvent(
        QPaintEvent *event /**< paint event flags */
        )
{
    QPainter painter;
    QListIterator<Impl::Point> pointIter(impl->points);
    const Impl::Point *point;
    int curX, curY, lastX, lastY;
    double xScaleFactor, yScaleFactor;

    QFrame::paintEvent(event); // draw frame
    painter.begin(this);

    painter.save();
    impl->xScale.draw(painter, impl->rect[X], QColor());
    painter.restore();
    painter.save();
    impl->yScale.draw(painter, impl->rect[Y], QColor());
    painter.restore();

    if (impl->points.count() <= 1 || !impl->xScale.range()
            || !impl->yScale.range())
        return;

    xScaleFactor = impl->graphRect.width() / impl->xScale.range();
    yScaleFactor = impl->graphRect.height() / impl->yScale.range();

    QPen pen(painter.pen());
    pen.setColor(impl->lineColor);
    pen.setWidth(impl->lineWidth);
    painter.setPen(pen);

    point = &pointIter.next();
    lastX = (int) ((point->value[X] - impl->xScale.getMin()) * xScaleFactor);
    lastY = (int) ((point->value[Y] - impl->yScale.getMin()) * yScaleFactor);

    while (pointIter.hasNext()) {
        point = &pointIter.next();
        curX =
            (int) ((point->value[X] - impl->xScale.getMin()) * xScaleFactor);
        curY =
            (int) ((point->value[Y] - impl->yScale.getMin()) * yScaleFactor);

        painter.drawLine(impl->graphRect.left() + lastX,
                impl->graphRect.bottom() - lastY,
                impl->graphRect.left() + curX,
                impl->graphRect.bottom() - curY);
        lastX = curX;
        lastY = curY;
    }
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

XYGraph::Impl::Impl(XYGraph *graph):
    graph(graph),
    axes{ new Axis(this), new Axis(this) },
    xScale(graph, Scale::Horizontal),
    yScale(graph, Scale::Vertical),
    lineWidth(DEFAULT_LINE_WIDTH),
    lineColor(DEFAULT_LINE_COLOR)
{
    timeRange = DEFAULT_TIMERANGE;
    xScale.setMin(DEFAULT_SCALEXMIN);
    xScale.setMax(DEFAULT_SCALEXMAX);
    yScale.setMin(DEFAULT_SCALEYMIN);
    yScale.setMax(DEFAULT_SCALEYMAX);
}

/****************************************************************************/

XYGraph::Impl::~Impl()
{
    for (int index = X; index < NumAxes; index++) {
        delete axes[index];
    }
}

/****************************************************************************/

/** Calculates the widget layout.
 */
void XYGraph::Impl::updateRects()
{
    rect[Y] = contRect;
    rect[Y].setTop(contRect.top() + xScale.getOuterLength() + 1);

    if (rect[Y].height() != yScale.getLength()) {
        yScale.setLength(rect[Y].height());
    }

    rect[X] = contRect;
    rect[X].setLeft(contRect.left() + yScale.getOuterLength() + 1);

    if (rect[X].width() != xScale.getLength()) {
        xScale.setLength(rect[X].width());
    }

    graphRect = contRect;
    graphRect.setLeft(rect[X].left());
    graphRect.setTop(rect[Y].top());

    graph->update();
}

/****************************************************************************/

/** Extracts graph points from the horizontal and vertical value lists.
 */
void XYGraph::Impl::extractPoints()
{
    const TimeValuePair *x, *y;
    Point p;
    int oldPointCount;

    if (axes[X]->values.count() && axes[Y]->values.count()) {
        oldPointCount = points.count();
        x = &axes[X]->values.first();
        y = &axes[Y]->values.first();

        while (1) {
            if (x->time == y->time) {
                p.value[X] = x->value;
                p.value[Y] = y->value;
                p.time = x->time;
                points.append(p);
                axes[X]->values.removeFirst();
                axes[Y]->values.removeFirst();
                if (!axes[X]->values.count() || !axes[Y]->values.count())
                    break;
                x = &axes[X]->values.first();
                y = &axes[Y]->values.first();
            }
            else if (x->time < y->time) {
                if (axes[X]->values.count() == 1) {
                    // only one x value before y
                    break;
                }

                if (axes[X]->values.first().time > y->time) {
                    // x is before y, next x ist after y
                    p.value[X] = x->value;
                    p.value[Y] = y->value;
                    p.time = y->time;
                    points.append(p);
                }

                axes[X]->values.removeFirst();
                x = &axes[X]->values.first();
            }
            else { // y->time < x->time
                if (axes[Y]->values.count() == 1) {
                    // only one y value before x
                    break;
                }

                if (axes[Y]->values.first().time > x->time) {
                    // y is before x, next y ist after x
                    p.value[X] = x->value;
                    p.value[Y] = y->value;
                    p.time = x->time;
                    points.append(p);
                }

                axes[Y]->values.removeFirst();
                y = &axes[Y]->values.first();
            }
        }

        if (points.count() != oldPointCount) {
            graph->update();
        }
    }

    removeDeprecated();
}

/****************************************************************************/

/** Removed values and points, that are older than specified in #timeRange.
 */
void XYGraph::Impl::removeDeprecated()
{
    unsigned int index;
    int oldPointCount;

    for (index = X; index < NumAxes; index++) {
        axes[index]->removeDeprecated();
    }

    if (points.count()) {
        oldPointCount = points.count();
        std::chrono::nanoseconds range{(int64_t) (timeRange * 1e9)};
        auto depTime(points.last().time - range);
        while (points.count() && points.first().time < depTime)
            points.removeFirst();
        if (points.count() != oldPointCount) {
            graph->update();
        }
    }
}

/****************************************************************************/

/** Axis constructor.
 */
XYGraph::Impl::Axis::Axis(XYGraph::Impl *impl):
    impl{impl}
{
}

/****************************************************************************/

/** This virtual method is called by the ProcessVariable, if its value
 * changes.
 */
void XYGraph::Impl::Axis::newValues(std::chrono::nanoseconds ts)
{
    TimeValuePair timeVal;

    timeVal.time = ts;
    PdCom::details::copyData(&timeVal.value,
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            getData(), getVariable().getTypeInfo().type, 1);
    timeVal.value = timeVal.value * scale + offset;

    values.append(timeVal);
    impl->extractPoints();
}

/****************************************************************************/

/** Notification for subscription cancellation forced by the ProcessVariable.
 *
 * This virtual function is called by the ProcessVariable, for any
 * subscription that is still active, when the ProcessVariable is about to be
 * destroyed.
 */
void XYGraph::Impl::Axis::stateChange(PdCom::Subscription::State state)
{
    if (state != PdCom::Subscription::State::Active) {
        values.clear();
    }
}

/****************************************************************************/

/** Removed values, that are older than specified in #timeRange.
 */
void XYGraph::Impl::Axis::removeDeprecated()
{
    if (values.count()) {
        std::chrono::nanoseconds range{(int64_t) (impl->timeRange * 1e9)};
        std::chrono::nanoseconds depTime{values.last().time - range};
        while (values.count() && values.first().time < depTime) {
            values.removeFirst();
        }
    }
}

/****************************************************************************/

/** Retranslate the widget.
 */
void XYGraph::Impl::retranslate()
{
    graph->setWindowTitle(Pd::XYGraph::tr("XY Graph"));
}

/****************************************************************************/
