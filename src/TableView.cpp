/*****************************************************************************
 *
 * Copyright (C) 2012  Florian Pose <fp@igh-essen.com>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableView.h"
#include "TableViewImpl.h"

using Pd::TableView;
using Pd::TableViewPrivate;

#include <QtPdCom1/TableModel.h>
using QtPdCom::TableModel;

#include <QCheckBox>
#include <QComboBox>
#include <QFileDialog>
#include <QFile>
#include <QGridLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QLayout>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QSpinBox>

// needs global namespace, so we use a trampoline
static void loadResource()
{
    Q_INIT_RESOURCE(QtPdWidgets);
}

/****************************************************************************/

/** Constructor.
 */
TableView::TableView(
        QWidget *parent /**< parent widget */
        ): QTableView(parent),
    d_ptr(new TableViewPrivate(this))
{
    loadResource();

    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
}

/****************************************************************************/

/** Constructor.
 */
TableViewPrivate::TableViewPrivate(TableView *parent)
    : q_ptr(parent)
{

    commitAction = new QAction(parent);
    commitAction->setEnabled(false);
    commitAction->setIcon(QIcon(":/QtPdWidgets/images/document-save.png"));
    QObject::connect(commitAction, SIGNAL(triggered()), parent, SLOT(commit()));

    revertAction = new QAction(parent);
    revertAction->setEnabled(false);
    revertAction->setIcon(QIcon(":/QtPdWidgets/images/edit-clear.png"));
    revertAction->setShortcut(Qt::Key_Escape);
    QObject::connect(revertAction, SIGNAL(triggered()), parent, SLOT(revert()));

    addRowAction = new QAction(parent);
    addRowAction->setEnabled(false);
    //fixme add ICON Hm,2013-12-11
    QObject::connect(addRowAction, SIGNAL(triggered()), parent, SLOT(addRow()));

    removeRowAction = new QAction(parent);
    removeRowAction->setEnabled(false);
    //fixme add ICON Hm,2013-12-11
    QObject::connect(removeRowAction, SIGNAL(triggered()), parent, SLOT(removeRow()));

    exportAsCsvAction = new QAction(parent);
    exportAsCsvAction->setEnabled(false);
    QObject::connect(exportAsCsvAction, &QAction::triggered,
            parent, &TableView::exportCsv);

    importFromCsvAction = new QAction(parent);
    importFromCsvAction->setEnabled(false);
    QObject::connect(importFromCsvAction, &QAction::triggered,
            parent, &TableView::importCsv);

    retranslate();
}

/****************************************************************************/

/** Destructor.
 */
TableView::~TableView()
{
}

/****************************************************************************/

/** Event handler.
 */
bool TableView::event(
        QEvent *event /**< Paint event flags. */
        )
{
    Q_D(TableView);

    if (event->type() == QEvent::LanguageChange) {
        d->retranslate();
    }

    return QTableView::event(event);
}

/****************************************************************************/

/** Handles keybord events from the user.
 *
 * Overloads the keyPressEvent handler from the parent class.
 */
void TableView::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
        case Qt::Key_Escape:
            revert();
            event->accept();
            break;
        case Qt::Key_Delete:
            if (const auto sm = selectionModel()) {
                if (const auto m = qobject_cast<QtPdCom::TableModel*>(model()))
                {
                    QList<QPersistentModelIndex> list;
                    for (const auto selected : sm->selectedRows()) {
                        list.append(QPersistentModelIndex(selected));
                    }
                    for (const auto &i : list) {
                        m->removeRows(i.row(), 1);
                    }
                    event->accept();
                }
            }
            break;
    }
}

/****************************************************************************/

/** Shows the context menu.
 */
void TableView::contextMenuEvent(QContextMenuEvent *event)
{
    Q_D(TableView);
    QAbstractItemModel *m = model();

    if (!m || !m->inherits("QtPdCom::TableModel")) {
        return;
    }

    bool editing = dynamic_cast<TableModel *>(m)->isEditing();

    d->commitAction->setEnabled(editing);
    d->revertAction->setEnabled(editing);

    unsigned int rowCapacity =
        static_cast<TableModel *>(m)->getRowCapacity();
    unsigned int rowCount = m->rowCount(QModelIndex());
    bool rowCountChangeable =
        static_cast<TableModel *>(m)->hasVisibleRowsVariable();

    d->addRowAction->setEnabled(
            rowCountChangeable && rowCapacity > 0);
    d->removeRowAction->setEnabled(
            rowCountChangeable && rowCount > 1);

    d->exportAsCsvAction->setEnabled(!editing && rowCount > 0);
    d->importFromCsvAction->setEnabled(m->columnCount() > 0);

    QMenu menu(this);
    menu.addAction(d->commitAction);
    menu.addAction(d->revertAction);
    menu.addAction(d->addRowAction);
    menu.addAction(d->removeRowAction);
    menu.addAction(d->importFromCsvAction);
    menu.addAction(d->exportAsCsvAction);
    menu.exec(event->globalPos());
}

/****************************************************************************/

/** Commits edited data.
 */
void TableView::commit()
{
    QAbstractItemModel *m = model();

    if (m && m->inherits("QtPdCom::TableModel")) {
        dynamic_cast<TableModel *>(m)->commit();
    }
}

/****************************************************************************/

/** Reverts edited data.
 */
void TableView::revert()
{
    if (model()) {
        model()->revert();
    }
}

/****************************************************************************/

/** Add a row.
 */
void TableView::addRow()
{
    QAbstractItemModel *m = model();

    if (m && m->inherits("QtPdCom::TableModel")) {
        static_cast<TableModel *>(m)->addRow();
    }
}

/****************************************************************************/

/** Remove a row.
 */
void TableView::removeRow()
{
    QAbstractItemModel *m = model();

    if (m && m->inherits("QtPdCom::TableModel")) {
        static_cast<TableModel *>(m)->remRow();
    }
}

/****************************************************************************/

void Pd::TableView::exportCsv()
{
    auto dialog = new QFileDialog(this,
            tr("Export as CSV"), "", tr("CSV Files (*.csv)"));

    dialog->setOption(QFileDialog::DontUseNativeDialog);
    dialog->setAcceptMode(QFileDialog::AcceptSave);
    dialog->setFileMode(QFileDialog::AnyFile);

    auto layout = qobject_cast<QGridLayout*>(dialog->layout());
    if (!layout) {
        qCritical("FileDialog Layout is not a GridLayout");
        return;
    }
    auto lb1 = new QLabel(dialog);
    lb1->setText(tr("CSV headers:"));
    layout->addWidget(lb1, layout->rowCount(), 0);
    auto cb = new QCheckBox(tr("Include column headers"), dialog);
    cb->setChecked(true);
    layout->addWidget(cb, layout->rowCount() - 1, 1);

    auto lb2 = new QLabel(dialog);
    lb2->setText(tr("CSV separator:"));
    layout->addWidget(lb2, layout->rowCount(), 0);
    auto comboBox = new QComboBox(dialog);
    comboBox->addItem(tr("Comma (,)"), ",");
    comboBox->addItem(tr("Semicolon (;)"), ";");
    comboBox->addItem(tr("Pipe (|)"), "|");
    layout->addWidget(comboBox, layout->rowCount() - 1, 1);

    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    connect(dialog, &QFileDialog::fileSelected, this,
            [this, cb, comboBox](const QString &fileName) {
                    QFile file(fileName);
                    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                        QMessageBox::critical(
                                this,
                                tr("Export failed"),
                                tr("Creating CSV file failed."));
                        return;
                    }
                    auto m = qobject_cast<QtPdCom::TableModel*>(model());
                    if (!m)
                        return;

                    const QChar sep =  comboBox->currentData().toString()[0];
                    file.write(m->toCsv(cb->isChecked(), sep).toUtf8());
    });

    dialog->show();
}

/****************************************************************************/

void Pd::TableView::importCsv()
{
    auto dialog = new QFileDialog(this,
            tr("Import from CSV"), "", tr("CSV Files (*.csv)"));

    dialog->setOption(QFileDialog::DontUseNativeDialog);
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::ExistingFile);

    auto layout = qobject_cast<QGridLayout*>(dialog->layout());
    if (!layout) {
        qCritical("FileDialog Layout is not a GridLayout");
        return;
    }

    auto lb = new QLabel(dialog);
    lb->setText(tr("CSV separator:"));
    layout->addWidget(lb, layout->rowCount(), 0);
    auto comboBox = new QComboBox(dialog);
    comboBox->addItem(tr("Comma (,)"), ",");
    comboBox->addItem(tr("Semicolon (;)"), ";");
    comboBox->addItem(tr("Pipe (|)"), "|");
    layout->addWidget(comboBox, layout->rowCount() - 1, 1);

    auto lb1 = new QLabel(dialog);
    lb1->setText(tr("First Row (zero based):"));
    layout->addWidget(lb1, layout->rowCount(), 0);
    auto spRow = new QSpinBox(dialog);
    spRow->setValue(1);
    layout->addWidget(spRow, layout->rowCount() - 1, 1);

    auto lb2 = new QLabel(dialog);
    lb2->setText(tr("First Column (zero based):"));
    layout->addWidget(lb2, layout->rowCount(), 0);
    auto spCol = new QSpinBox(dialog);
    layout->addWidget(spCol, layout->rowCount() - 1, 1);

    connect(dialog, &QDialog::finished, dialog, &QObject::deleteLater);
    connect(dialog, &QFileDialog::fileSelected, this,
            [this, comboBox, spRow, spCol](const QString &fileName) {
                    QFile file(fileName);
                    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                        QMessageBox::critical(
                                this,
                                tr("Import failed"),
                                tr("Reading CSV file failed."));
                        return;
                    }
                    auto m = qobject_cast<QtPdCom::TableModel*>(model());
                    if (!m)
                        return;

                    const QChar sep = comboBox->currentData().toString()[0];
                    const auto data = QString::fromLocal8Bit(file.readAll());
                    const auto lines = m->fromCsv(data, sep, spRow->value(), spCol->value());
                    if (lines < 0)
                        QMessageBox::critical(this, tr("CSV import"),
                                tr("Failed to import CSV file."));
                    else
                        QMessageBox::information(this, tr("CSV import"),
                                tr("Imported %1 rows.").arg(lines));
    });

    dialog->show();
}

/****************************************************************************/

/** retranslate
 */
void TableViewPrivate::retranslate()
{
    commitAction->setText(Pd::TableView::tr("&Commit"));
    commitAction->setStatusTip(
            Pd::TableView::tr("Commit edited data to process."));
    revertAction->setText(Pd::TableView::tr("&Revert"));
    revertAction->setStatusTip(
            Pd::TableView::tr("Revert edited data."));

    addRowAction->setText(Pd::TableView::tr("&Add Row"));
    addRowAction->setStatusTip(
	    Pd::TableView::tr("Append a row to the table."));

    removeRowAction->setText(Pd::TableView::tr("&Remove Row"));
    removeRowAction->setStatusTip(
	    Pd::TableView::tr("Remove last row from table."));

    exportAsCsvAction->setText(Pd::TableView::tr("Export as CSV..."));
    exportAsCsvAction->setStatusTip(
            Pd::TableView::tr("Export table data as csv file."));

    importFromCsvAction->setText(
            Pd::TableView::tr("Import from CSV..."));
    importFromCsvAction->setStatusTip(
            Pd::TableView::tr("Import table data from a csv file."));
}

/****************************************************************************/
