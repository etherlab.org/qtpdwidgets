/*****************************************************************************
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *               2023 Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_PARAMETERFILESYSTEMMODEL_H
#define PD_PARAMETERFILESYSTEMMODEL_H

#include <chrono>
#include <QAbstractListModel>
#include <QFileInfo>

namespace Pd
{
    class ParameterFileSystemModel:
        public QAbstractListModel
    {
        Q_OBJECT

      public:
        static constexpr auto DELETE_DELAY = std::chrono::seconds{1};

        explicit ParameterFileSystemModel(QObject *parent = nullptr);
        ~ParameterFileSystemModel();

        // change directory path and reset model
        void setPath(const QString path);

        int rowCount(const QModelIndex &) const override;
        QVariant data(const QModelIndex &, int) const override;
        Qt::ItemFlags flags(const QModelIndex &) const override;

        const QFileInfo *getFileInfo(const QModelIndex &idx) const
        {
            if (idx.isValid() && idx.row() >= 0 && idx.row() < files.size())
            {
                return &files.at(idx.row());
            }
            return nullptr;
        }
        const QFileInfo *getFileInfo(int row) const
        {
            if (row >= 0 && row < files.size())
            {
                return &files.at(row);
            }
            return nullptr;
        }

      public slots:
        void refresh();
        void removeReallyDeletedFiles();


      private:
        class FileInfoWithTimer : public QFileInfo {
          public:
            FileInfoWithTimer(QFileInfo const& fi) : QFileInfo(fi) {}

            bool delete_delay_timer_running_ = false;
            bool is_really_deleted_ = false;
        };

        class FileInfoList : public QList<FileInfoWithTimer>
        {
          public:
            FileInfoList() = default;
            FileInfoList(const QFileInfoList& list)
            {
                for (const auto& l : list) {
                    append(l);
                }
            }
        };

        QString path_;
        FileInfoList files; // elements are QFileInfo

        QFileInfoList getInfo() const;
        void addItems(const QFileInfoList& items);
        void startDeleteDelayTimer(const QPersistentModelIndex& idx);
    };

    /************************************************************************/

}  // namespace Pd

#endif
