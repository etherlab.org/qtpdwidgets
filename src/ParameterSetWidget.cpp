/*****************************************************************************
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "QtPdWidgets2/ParameterSetWidget.h"

#include "Parameter.h"
#include "ParameterFileSystemModel.h"

#include <QAction>
#include <QComboBox>
#include <QContextMenuEvent>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFileSystemModel>
#include <QFileSystemWatcher>
#include <QHBoxLayout>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLabel>
#include <QListView>
#include <QMenu>
#include <QPushButton>
#include <QtGlobal>

using Pd::ParameterSetWidget;

/****************************************************************************/

struct ParameterSetWidget::Impl
{
    Impl(ParameterSetWidget *widget);
    ~Impl();

    void loadDialog();

    void connectParameters();
    void applyParameters();
    void loadParameters(int index);
    void clearParameters();

    void updateProcessPixmap();
    void compareValues();
    void fromJson(const QJsonArray &array);
    bool fileError() const;

    ParameterSetWidget * const widget;

    QHBoxLayout layout;
    QLabel label;
    QComboBox comboBox;
    QPushButton applyButton;

    QList<Pd::Parameter *> parameters;
    QPixmap processPixmap;
    ParameterFileSystemModel boxModel;
    QListView boxModelView;

    QString path; /**< Directory path. */
    QSet<QtPdCom::Process *> processes;
    QFileSystemWatcher directoryWatcher;
    QFileSystemWatcher currentFileWatcher;

    bool different = true;
    bool formatError = false;
    bool noData = false;
};

/****************************************************************************/

/** Constructor.
 */
ParameterSetWidget::ParameterSetWidget(
        QWidget *parent /**< parent widget */):
    QFrame(parent),
    impl(std::unique_ptr<Impl>(new Impl(this)))
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
}

/****************************************************************************/

/** Destructor.
 */
ParameterSetWidget::~ParameterSetWidget()
{
}

/****************************************************************************/

void ParameterSetWidget::setProcesses(QSet<QtPdCom::Process *> processes)
{
    // clear process deletion signal connections
    for (const auto p: impl->processes) {
        disconnect(p, nullptr, this, nullptr);
    }
    impl->processes = processes;
    // remove process from set when deleted.
    for (const auto p: impl->processes) {
        connect(p, &QObject::destroyed, this, [p, this]() {
            impl->processes.remove(p);
        });
    }
    impl->connectParameters();
    impl->updateProcessPixmap();
}

/****************************************************************************/

void ParameterSetWidget::setPath(const QString &path)
{
    if (not impl->directoryWatcher.files().empty()) {
        impl->directoryWatcher.removePath(impl->path);
    }

    impl->path = path;
    impl->boxModel.setPath(path);
    impl->comboBox.setEnabled(true);

    impl->directoryWatcher.addPath(path);
}

/****************************************************************************/

QString ParameterSetWidget::getPath() const
{
    return impl->path;
}

/****************************************************************************/

bool ParameterSetWidget::event(QEvent *event)
{
    if (event->type() == QEvent::ContextMenu) {
        QContextMenuEvent *menuEvent =
            static_cast<QContextMenuEvent *>(event);

        QMenu menu;

        QAction *action;

        action = new QAction(this);
        action->setText(tr("Change Folder..."));
        action->setIcon(QIcon(":/QtPdWidgets/images/document-open.svg"));
        connect(action, &QAction::triggered,
                this, [this]() { impl->loadDialog(); });
        menu.addAction(action);

        menu.exec(menuEvent->globalPos());

        return true;
    }

    return QFrame::event(event);
}

/****************************************************************************/

QSize ParameterSetWidget::sizeHint() const
{
    return QSize(100, impl->layout.sizeHint().height());
}

/*****************************************************************************
 * Implementation
 ****************************************************************************/

ParameterSetWidget::Impl::Impl(ParameterSetWidget *widget):
    widget(widget),
    layout(widget),
    label(widget),
    comboBox(widget),
    applyButton(widget),
    boxModel(widget)
{
    layout.addWidget(&label, 0);
    updateProcessPixmap();

    boxModelView.setModel(&boxModel);
    boxModelView.setSpacing(1);
    comboBox.setModel(&boxModel);
    comboBox.setView(&boxModelView);
    comboBox.setInsertPolicy(QComboBox::NoInsert);
    comboBox.setEditable(false);
    comboBox.setEnabled(false);
    comboBox.setSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
    QObject::connect(
            &comboBox,
            QOverload<int>::of(&QComboBox::currentIndexChanged), widget,
            [this](int index) { loadParameters(index);boxModel.removeReallyDeletedFiles(); });
    layout.addWidget(&comboBox, 1);

    applyButton.setIcon(
            QIcon(":/QtPdWidgets/images/apply-parameters.svg"));
    applyButton.setEnabled(false);
    QObject::connect(
            &applyButton, &QPushButton::clicked,
            [this]() { applyParameters(); });
    layout.addWidget(&applyButton, 0);

    QObject::connect(
            &directoryWatcher, &QFileSystemWatcher::directoryChanged,
            &boxModel, &Pd::ParameterFileSystemModel::refresh);
    QObject::connect(
            &currentFileWatcher, &QFileSystemWatcher::fileChanged, widget,
            [this](const QString &path) {
                const int index = comboBox.currentIndex();
                if (const auto fi = boxModel.getFileInfo(index)) {
                    if (*fi == QFileInfo(path)) {
                        loadParameters(index);
                    }
                }
            });

    QObject::connect(&boxModel, &QAbstractItemModel::dataChanged, widget,
        [this](const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &/* roles*/)
        {
            if (topLeft.row() <= comboBox.currentIndex() && comboBox.currentIndex() <= bottomRight.row()) {
                loadParameters(comboBox.currentIndex());
            }
            updateProcessPixmap();
        });

    layout.setSizeConstraint(QLayout::SetMinimumSize);
    layout.setContentsMargins(0,0,0,0);
}

/***************************************************************************/

ParameterSetWidget::Impl::~Impl()
{
    QObject::disconnect(&directoryWatcher, &QFileSystemWatcher::directoryChanged,
            nullptr, nullptr);
    clearParameters();
}

/***************************************************************************/

void ParameterSetWidget::Impl::loadDialog()
{
    QFileDialog* dialog = new QFileDialog(widget);
    dialog->setAcceptMode(QFileDialog::AcceptOpen);
    dialog->setFileMode(QFileDialog::Directory);
    dialog->setOption(QFileDialog::ShowDirsOnly, true);

    QObject::connect(dialog, &QDialog::finished, dialog,
            &QObject::deleteLater);
    QObject::connect(dialog, &QDialog::accepted, widget, [this, dialog]()
            {
                widget->setPath(dialog->selectedFiles()[0]);
            }
    );

    dialog->open();
}

/***************************************************************************/

void ParameterSetWidget::Impl::connectParameters()
{
    foreach (Parameter *par, parameters) {
        QUrl parameterUrl(par->getUrl());
        QtPdCom::Process *process(nullptr);
        for(const auto proc : processes) {
            const auto process_url = proc->getUrl();
            if (parameterUrl.host() == process_url.host() &&
                    parameterUrl.port() == process_url.port()) {
                process = proc;
                break;
            }
        }
        if (process) {
            par->connectParameter(process);
        }
        else {
            qWarning() << "No process found for" << parameterUrl;
        }
    }
    for (const auto process : processes)
        process->callPendingCallbacks();
}

/***************************************************************************/

void ParameterSetWidget::Impl::applyParameters()
{
    for (const auto par: parameters) {
        par->setCurrentValue(par->getSavedValue());
    }
}

/***************************************************************************/

void ParameterSetWidget::Impl::loadParameters(int index)
{
#ifdef DEBUG_PD_PARAMETERSETWIDGET
    qDebug() << __func__ << index;
#endif
    const auto fi = boxModel.getFileInfo(index);
    if (!currentFileWatcher.files().empty())
        currentFileWatcher.removePaths(currentFileWatcher.files());
    formatError = true;

    if (!fi) {
        applyButton.setEnabled(false);
        return;
    }

    const QString currentPath(fi->filePath());

    if (currentPath.isEmpty()) {
        applyButton.setEnabled(false);
        return;
    }

    QFile file(currentPath);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << tr("Failed to open: %1").arg(currentPath);
        qWarning() << tr("Reason: %1").arg(file.errorString());
        return;
    }

    QByteArray ba = file.readAll();

    QJsonParseError err;
    QJsonObject layoutObject;

    if (!ba.isEmpty()) {
        layoutObject = QJsonDocument::fromJson(ba, &err).object();
    } else {
        qWarning() << "File content is empty.";
    }

#ifdef DEBUG_PD_PARAMETERSETWIDGET
    qDebug() << "current path" << currentPath;
    qDebug() << "file name" << file.fileName();
    qDebug() << "file readable" << file.isReadable();
    qDebug() << "file open" << file.isOpen();
    qDebug() << "ba empty" << ba.isEmpty() << "\n";
#endif
    file.close();
    formatError = err.error != QJsonParseError::NoError;

    if (formatError) {
        qCritical() << "Parameter file parsing error (" << err.error
            << ") at offset " << err.offset << ": "
            << err.errorString();
        updateProcessPixmap();
        return;
    }

    QJsonArray parameterArray(layoutObject["parameters"].toArray());

    clearParameters();
    fromJson(parameterArray);

    if (!currentFileWatcher.addPath(currentPath))
    {
        qWarning() << "Failed to watch" << currentPath;
    }

    connectParameters();
    compareValues();
}

/***************************************************************************/

void ParameterSetWidget::Impl::clearParameters()
{
    foreach (Parameter *par, parameters) {
        delete par;
    }
    parameters.clear();
    for (const auto process : processes) {
        process->callPendingCallbacks();
    }
}

/***************************************************************************/

void ParameterSetWidget::Impl::updateProcessPixmap()
{
    QString image;
    bool anyError = processes.empty() or fileError() or noData or formatError;
#ifdef DEBUG_PD_PARAMETERSETWIDGET
        qDebug() << __FILE_NAME__ << __func__ << "different" << different
    << "anyError" << anyError
    << "emptyProcesses" << emptyProcesses
    << "fileError" << fileError
    << "formatError" << formatError
    << "noData" << noData;
#endif

    if (different or anyError) {
        image = ":/QtPdWidgets/images/parameters-differing.svg";
    }
    else {
        image = ":/QtPdWidgets/images/parameters-equal.svg";
    }
    applyButton.setEnabled(different and (not anyError));
    processPixmap.load(image);
    label.setPixmap(
            processPixmap.scaledToHeight(30, Qt::SmoothTransformation));
}

/***************************************************************************/

void ParameterSetWidget::Impl::compareValues()
{
    different = false;
    noData = false;
    foreach (Parameter *par, parameters) {
#ifdef DEBUG_PD_PARAMETERSETWIDGET
        qDebug() << __func__ << par->getUrl() << par->hasData()
            << par->getCurrentValue() << par->getSavedValue();
#endif
        if (not par->hasData()) {
            noData = true;
            break;
        } else if (par->getCurrentValue() != par->getSavedValue()) {
            different = true;
            break;
        }
    }
    updateProcessPixmap();
}

/***************************************************************************/

void ParameterSetWidget::Impl::fromJson(const QJsonArray &array)
{
    foreach (QJsonValue arrayValue, array) {
        Parameter *parameter = new Parameter(widget);
        parameter->fromJson(arrayValue);
        parameters.append(parameter);

#ifdef DEBUG_PD_PARAMETERSETWIDGET
        qDebug() << __func__ << "loaded" << parameter->getUrl();
#endif

        QObject::connect(parameter, &Parameter::dataChanged,
                [this]() { compareValues(); });
    }
}

bool ParameterSetWidget::Impl::fileError() const
{
    const auto fi = boxModel.getFileInfo(comboBox.currentIndex());
    if (!fi || !fi->exists())
        return true;
    return false;
}

/***************************************************************************/
