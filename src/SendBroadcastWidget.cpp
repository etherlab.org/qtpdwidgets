/*****************************************************************************
 *
 * Copyright (C) 2009 - 2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "SendBroadcastWidget.h"

#include <QCoreApplication>
#include <QEvent>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QtPdCom1/Process.h>

namespace Pd {
class SendBroadcastWidgetPrivate
{
    friend class SendBroadcastWidget;

    explicit SendBroadcastWidgetPrivate(SendBroadcastWidget *p) :
        lineEdit(new QLineEdit(p)), pbSend(new QPushButton(p))
    {}

    Q_DECLARE_TR_FUNCTIONS(SendBroadcastWidgetPrivate);

    void retranslateUi() { pbSend->setText(tr("Send")); }

    QLineEdit *const lineEdit;
    QPushButton *const pbSend;
    QtPdCom::Process *process = nullptr;
    QString attributeName     = "text";
};
}  // namespace Pd

using Pd::SendBroadcastWidget;

SendBroadcastWidget::SendBroadcastWidget(QWidget *parent) :
    QWidget {parent}, d_ptr(new Pd::SendBroadcastWidgetPrivate(this))
{
    Q_D(SendBroadcastWidget);

    d->retranslateUi();

    auto layout = new QHBoxLayout(this);
    layout->addWidget(d->lineEdit);
    layout->addWidget(d->pbSend);

    const auto do_send = [d]() {
        if (d->process && d->process->isConnected()) {
            d->process->sendBroadcast(d->lineEdit->text());
            d->lineEdit->clear();
        }
    };

    connect(d->pbSend, &QPushButton::clicked, this, do_send);
    connect(d->lineEdit, &QLineEdit::returnPressed, this, do_send);
}

SendBroadcastWidget::~SendBroadcastWidget() = default;

void Pd::SendBroadcastWidget::setProcess(QtPdCom::Process *p)
{
    Q_D(SendBroadcastWidget);

    d->process = p;
}

QString Pd::SendBroadcastWidget::getAttributeName() const
{
    const Q_D(SendBroadcastWidget);

    return d->attributeName;
}

void Pd::SendBroadcastWidget::setAttributeName(QString name)
{
    Q_D(SendBroadcastWidget);
    d->attributeName = name;
}

void SendBroadcastWidget::changeEvent(QEvent *event)
{
    Q_D(SendBroadcastWidget);

    if (event && event->type() == QEvent::LanguageChange) {
        d->retranslateUi();
    }
    QWidget::changeEvent(event);
}

QtPdCom::Process *SendBroadcastWidget::getProcess() const
{
    const Q_D(SendBroadcastWidget);

    return d->process;
}
