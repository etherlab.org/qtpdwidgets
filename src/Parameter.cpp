/*****************************************************************************
 *
 * Copyright (C) 2023 Daniel Ramirez <dr@igh.de>
 *               2023 Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdWidgets library.
 *
 * The QtPdWidgets library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdWidgets library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdWidgets Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Parameter.h"

#include "QtPdWidgets2/ParameterSetWidget.h"

#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>

using Pd::Parameter;

/*****************************************************************************
 * public
 ****************************************************************************/

Parameter::Parameter(Pd::ParameterSetWidget *widget) :
    PdCom::Subscriber(PdCom::event_mode),
    widget(widget), 
    scale {1.0},
    offset {0.0},
    dataPresent(false), 
    savedDataPresent(false)
{}

/****************************************************************************/

Parameter::~Parameter()
{}

/****************************************************************************/

void Parameter::fromJson(const QJsonValue &parameter)
{
    QJsonObject obj(parameter.toObject());

    url = obj["url"].toString();
    connectionPath =
        url.adjusted(QUrl::RemoveAuthority | QUrl::RemoveScheme).toString();

    double scale{1.0};
    if (obj.contains("scale")) {
        auto scaleValue(obj["scale"]);
        if (not scaleValue.isDouble()) {
            throw(std::invalid_argument(
                    "scale must be a scalar double value."));
        }

        scale = obj["scale"].toDouble();
        if (scale == 0.0) {
            throw(std::invalid_argument("scale must not be zero"));
        }
    }

    double offset{0.0};
    if (obj.contains("offset")) {
        auto offsetValue(obj["offset"]);
        if (not offsetValue.isDouble()) {
            throw(std::invalid_argument(
                    "offset must be a scalar double value."));
        }

        offset = obj["offset"].toDouble();
    }

    savedItems = QVector<double>();
    savedDataPresent = false;

    if (obj.contains("value")) {
        auto value(obj["value"]);
        if (value.isArray()) {

            auto array(value.toArray());
            for (int i = 0; i < array.count(); i++) {
                if (not array.at(i).isDouble()) {
                    throw(std::invalid_argument(
                                "value must be a scalar"
                                " or array of doubles."));
                }
                savedItems.append((array.at(i).toDouble() - offset) / scale);
            }
            savedDataPresent = true;
        }
        else if (value.isDouble()) {
            savedItems.append((value.toDouble() - offset) / scale);
            savedDataPresent = true;
        }
        else {
            throw(std::invalid_argument(
                        "value must be a scalar or array of doubles."));
        }
    }
        
#ifdef DEBUG_PD_PARAMETER
    qWarning() << __func__ << "connection path" << connectionPath;
    qWarning() << __func__ << "saved value" << savedItems;
#endif
}

/****************************************************************************/

void Parameter::replaceUrl(const QUrl &oldUrl, const QUrl &newUrl)
{
    QUrl dataSourceUrl(url.adjusted(
            QUrl::RemovePassword | QUrl::RemovePath | QUrl::RemoveQuery
            | QUrl::RemoveFragment));
    if (dataSourceUrl != oldUrl) {
        return;
    }

    url.setScheme(newUrl.scheme());
    url.setAuthority(newUrl.authority());  // user, pass, host, port
}

/****************************************************************************/

void Parameter::connectParameter(QtPdCom::Process *p)
{
    if (p) {
        pv = p->find(connectionPath).result();

        // Subscribe to process variable as ScalarSubscriber
        subscription = PdCom::Subscription(*this, pv);

        try {
            subscription.poll();
        }
        catch (PdCom::Exception &e) {
            QString msg(tr("Parameter poll failed: %1").arg(e.what()));
            qWarning() << msg;
        }
#ifdef DEBUG_PD_PARAMETER
        qDebug() << __func__ << url;
        qDebug() << __func__ << p;
        qDebug() << __func__ << &pv;
        qDebug() << __func__ << &subscription;
#endif
    }
}

/***************************************************************************/

bool Parameter::setCurrentValue(const QVector<double> &value)
{
    if (pv.empty() or not dataPresent) {
        return false;
    }

    int nelem(pv.getSizeInfo().totalElements());
    if (nelem != value.size()) {
        return false;
    }

    pv.setValue(value.constData(),
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            nelem);
    return true;
}

/*****************************************************************************
 * private
 ****************************************************************************/

void Parameter::newValues(std::chrono::nanoseconds)
{
    auto pv(getVariable());
    auto nelem(pv->getSizeInfo().totalElements());

    QVector<double> values(nelem);
    PdCom::details::copyData(
            values.data(),
            PdCom::details::TypeInfoTraits<double>::type_info.type,
            subscription.getData(),
            pv->getTypeInfo().type,
            nelem);
    currentItems = values;
    dataPresent = true;
    emit dataChanged();
}

/****************************************************************************/

void Parameter::stateChanged(const PdCom::Subscription &sub)
{
#ifdef DEBUG_PD_PARAMETER
    qDebug() << __func__ << url << (int) sub.getState();
#endif
    if (sub.getState() != PdCom::Subscription::State::Active) {
        dataPresent = false;
        emit dataChanged();
    }
}

/****************************************************************************/
