[[_TOC_]]

# The QtPdWidgets Library

This is the QtPdWidgets library, a process-data aware widget library for Qt
from the EtherLab project. It is based on PdCom 5 and QtPdCom 1. You can find
the QtPdWidgets library on its
[GitLab Page](https://gitlab.com/etherlab.org/qtpdwidgets) or the
[EtherLab Homepage](http://etherlab.org/en/pdwidgets). There are binary
packages available on
[Open Build Service](https://build.opensuse.org/package/show/science:EtherLab/QtPdWidgets2).

# License

The QtPdWidgets library is licensed under the terms and conditions of the GNU
Lesser General Public License (LGPL), version 3 or (at your option) any later
version.

# Prerequisites

   - Qt5
   - [PdCom 5](https://gitlab.com/etherlab.org/pdcom)
   - [QtPdCom 1](https://gitlab.com/etherlab.org/qtpdcom)

# Building and Installing

The build process is handled by CMake. To build and install, call:

```bash
mkdir build
cd build
cmake ..
make -j$(nproc)
make install
```

To change the installation path,
call cmake with `-DCMAKE_INSTALL_PREFIX=/my/custom/prefix`.
If PdCom5 and/or QtPdCom1 is installed at a non-standard location,
use `-DCMAKE_PREFIX_PATH=/pdcom/install/prefix`.
Multiple paths can be joined by semicolon (`;`).
You can even use this option to point CMake to a build directory of
PdCom5 and/or QtPdCom1, this adds the possibility to use a version
of PdCom5 and/or QtPdCom1 which is not installed at all.

To import this library into your own application,
add the following to your `CMakeLists.txt`:
```cmake
find_package(QtPdWidgets2 REQUIRED)
target_link_libraries(your_application PUBLIC
   EtherLab::QtPdWidgets2
)
```
All dependencies are imported and linked automatically.
The `CMAKE_PREFIX_PATH` argument works here, too.



Have fun!
